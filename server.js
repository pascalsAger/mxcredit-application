
/**
 * Module dependencies.
 */

// Setup 
// =================
var express = require('express');
var app = express();
var path = require('path');
var neo4j = require('neo4j-js');
var bodyParser     = require('body-parser');
var methodOverride = require('method-override');


var userApi = require('./api/routes/userApi');
var componentApi = require('./api/routes/componentApi');
var subComponentApi = require('./api/routes/subComponentApi');
var itemApi = require('./api/routes/itemApi');
var groupApi = require('./api/routes/groupApi');
var pStepsApi = require('./api/routes/processStepsApi');
var documentApi = require('./api/routes/documentApi');
var processEngineApi = require('./api/routes/processEngineApi');
var fileApi = require('./api/routes/fileApi');
var linkApi = require('./api/routes/linkApi');
var stepGroupApi = require('./api/routes/pStepsUserGroupsApi.js');
var mbuiApi = require('./api/routes/mbuiApi.js');

var port = process.env.PORT || 4000; // set our port


app.use(bodyParser.json()); // parse application/json 
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(bodyParser.urlencoded({ extended: true })); // parse application/x-www-form-urlencoded
app.use(methodOverride('X-HTTP-Method-Override')); // override with the X-HTTP-Method-Override header in the request. simulate DELETE/PUT


app.use(express.static(path.join(__dirname, 'app')));

// We only need a default route.  Angular will handle the rest.
 app.get('/', function(request, response){
  response.sendfile("./app/index.html");
});


app.use('/api/user', userApi);
app.use('/api/component', componentApi);
app.use('/api/subComponent', subComponentApi);
app.use('/api/item', itemApi);
app.use('/api/group', groupApi);
app.use('/api/processSteps', pStepsApi);
app.use('/api/document', documentApi);
app.use('/api/processEngine', processEngineApi);
app.use('/api/file', fileApi);
app.use('/api/link', linkApi);
app.use('/api/stepGroup', stepGroupApi);
app.use('/api/processSchema', mbuiApi);
// start app ===============================================
app.listen(port);	
console.log('Server running on port ' + port);



