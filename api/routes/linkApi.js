var neo4j = require('neo4j-js');
var express = require('express');
var router = express.Router();


var neo4JUrl = 'http://127.0.0.1:7474/db/data/';




// get all items for a sub component and component
router.get('/GetLinks/:componentID/:processID', function (req, res) {
  var retValue = '';
    var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {    	    
    	     	 
         var componentID = req.params.componentID;
         var processID = req.params.processID;
         var  query = ["MATCH (component:Component { component_id: '"+componentID+"' })-[:HAS]-(mxCredit:MxCreditProcess)-[:HAS]-(pg:ProcessGroup)-[:HAS]-(process:ProcessStep{ID:"+processID+"})-[:HAS]- (link:Link)  return link"].join('\n');
         graph.query(query, null, function (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
            console.log(retValue);
        });       
    });
});

// creat link node to process 
router.post('/', function (req, res) {

console.log("-----------------add hoc-------------------------");
  var retValue = '';
  var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {
    	  
        var query = req.body.query;
        
        graph.query(query, null, function (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
        });
       
    });
console.log("-----------------end creat link node to process -------------------------");
});

// delete link by link node id 
router.delete('/:linkID', function (req, res) {

console.log("-----------------delete link by link node id -------------------------");
  var retValue = '';
    var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {
    	    
         var linkID = req.params.linkID;   
         var query = [  "MATCH (p:Link) where ID(p)= "+ linkID +" match (p)-[r]-() delete p,r"].join('\n');
         	 
         graph.query(query, null, function (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
            console.log(retValue.length);
        });
       
    });
console.log("-----------------end delete link-------------------------");
});



module.exports = router;