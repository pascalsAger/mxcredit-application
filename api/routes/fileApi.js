var neo4j = require('neo4j-js');
var express = require('express');
var router = express.Router();


var neo4JUrl = 'http://127.0.0.1:7474/db/data/';


// silly comment added

// get all items for a sub component and component
router.get('/GetFiles/:componentID/:processID', function (req, res) {
  var retValue = '';
    var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {    	    
    	     	 
         var componentID = req.params.componentID;
         var processID = req.params.processID;
         var  query = ["MATCH (component:Component { component_id: '"+componentID+"' })-[:HAS]-(mxCredit:MxCreditProcess)-[:HAS]-(pg:ProcessGroup)-[:HAS]-(process:ProcessStep{ID:"+processID+"})-[:HAS]- (dataFile:DataFile)  return dataFile"].join('\n');
         graph.query(query, null, function (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
            console.log(retValue);
        });       
    });
});

// creat file node to process 
router.post('/', function (req, res) {

console.log("-----------------add hoc-------------------------");
  var retValue = '';
  var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {
    	  
        var query = req.body.query;
        
        graph.query(query, null, function (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
        });
       
    });
console.log("-----------------end add hoc-------------------------");
});

// delete file by process file name 
router.delete('/:fileID', function (req, res) {

console.log("-----------------delete file by id-------------------------");
  var retValue = '';
    var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {
    	    
         var fileID = req.params.fileID;   
         var query = [  "MATCH (p:DataFile) where ID(p)= "+ fileID +" match (p)-[r]-() delete p,r"].join('\n');
         	 
         graph.query(query, null, function (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
            console.log(retValue.length);
        });
       
    });
console.log("-----------------end delete file by id-------------------------");
});



module.exports = router;