var neo4j = require('neo4j-js');
var express = require('express');
var router = express.Router();


var neo4JUrl = 'http://127.0.0.1:7474/db/data/';


// get all components of the System
router.get('/', function (req, res) {

  var retValue = '';
    var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {
    	    
         var  query = ['Match(stakeHolder:StakeHolder)-[:HAS_GROUP]->(group:Group) return group'].join('\n');
        // console.log(query);
         graph.query(query, null, function  (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
        });
       
    });
});

router.get('/:id', function (req, res) {
  var retValue = '';
    var error = ''; 
    neo4j.connect(neo4JUrl, function (err, graph) {
    	    
    	   var id = req.params.id;
    	   console.log(id);
         var  query = ["Match(group:Group) Where ID(group) = "+ id +" return group"].join('\n');
         console.log(query);
         graph.query(query, null, function (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
            //console.log(retValue);
        });
       
    });
});

// create group

	router.post('/', function(req, res) {

		console.log("-----------------add hoc-------------------------");

		neo4j.connect(neo4JUrl, function(err, graph) {

			var query = req.body.query;

			graph.query(query, null, function(error, results) {
				if (error) {
					error = error;
				} else {
					retValue = results;
				}
				res.json({
					responseData : retValue,
					error : error
				});
			});

		});
	}); 


// get all users of a group by userID
router.get('/allusers/:groupID', function (req, res) {
        var retValue = '';
    	var error = ''; 
    neo4j.connect(neo4JUrl, function (err, graph) {
    	    
    	  var id = req.params.groupID;
    	  console.log(id);
    	  var  query = ["Match(group:Group) Where ID(group) = "+ id +"  Match (group)-[:HAS_USER] ->(user:User) return user"].join('\n');
         
         graph.query(query, null, function (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
            //console.log(retValue);
        });
       
    });
});

module.exports = router;