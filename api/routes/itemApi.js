var neo4j = require('neo4j-js');
var express = require('express');
var router = express.Router();


var neo4JUrl = 'http://127.0.0.1:7474/db/data/';




// get all items for a sub component and component
router.post('/GetItems', function (req, res) {
  var retValue = '';
    var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {    	    
    	     	 
          var query = req.body.query;
         
         graph.query(query, null, function (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
            console.log(retValue);
        });       
    });
});

// create item
router.post('/', function (req, res) {

console.log("-----------------add hoc-------------------------");
  var retValue = '';
  var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {
    	  
        var query = req.body.query;
        
        graph.query(query, null, function (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
        });
       
    });
console.log("-----------------end add hoc-------------------------");
});

// delete item by node id
router.delete('/:itemID', function (req, res) {

console.log("-----------------delete item by id-------------------------");
  var retValue = '';
    var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {
    	    
         var itemID = req.params.itemID;   
         var query = [  "MATCH (p:Item) where ID(p)= "+ itemID +" match (p)-[r]-() delete p,r"].join('\n');
         	 
         graph.query(query, null, function (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
            console.log(retValue.length);
        });
       
    });
console.log("-----------------end delete item by id-------------------------");
});



module.exports = router;