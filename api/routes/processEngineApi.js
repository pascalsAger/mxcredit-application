var neo4j = require('neo4j-js');
var express = require('express');
var router = express.Router();
var neo4JUrl = 'http://127.0.0.1:7474/db/data/';


// get all active process of the component
router.get('/getActiveProcess/:componentId', function (req, res) {

console.log("-----------------get all components-------------------------");
  var retValue = '';
    var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {
    	 
    	 var componentId = req.params.componentId;  
    	 
         var  query = ["MATCH (component:Component { component_id: '"+ componentId +"' })-[:HAS]-(mxCredit:MxCreditProcess)-[:HAS]",
         "-(pg:ProcessGroup)-[:HAS]-(sourceProcess:ProcessStep{isActive:'TRUE'})-",
         "[sequenceFlow:SEQUENCE_FLOW]->(destinationProcess:ProcessStep)",
         "return sourceProcess,sequenceFlow,destinationProcess"].join('\n');;
        
         graph.query(query, null, function  (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
           // console.log(retValue.length);
        });
       
    });
console.log("-----------------end get all components-------------------------");
});

// get all active process of the component
router.get('/getProcessDetails/:componentId/:processStepId', function (req, res) {

console.log("-----------------get all process details-------------------------");
  var retValue = '';
    var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {
    	 
    	 var componentId = req.params.componentId;  
    	 var processStepId = req.params.processStepId;
    	 console.log(componentId);
    	 console.log(processStepId);
    	 
         var  query = ["MATCH (component:Component { component_id: '"+ componentId +"' })-[:HAS]-(mxCredit:MxCreditProcess)-[:HAS]-",
         "(pg:ProcessGroup)-[:HAS]-(sourceProcess:ProcessStep{ID:"+ processStepId +"})-[sequenceFlow:SEQUENCE_FLOW]->",
         "(destinationProcess:ProcessStep) return sourceProcess,sequenceFlow,destinationProcess"].join('\n');
        console.log(query);
         graph.query(query, null, function  (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
            console.log(retValue.length);
        });
       
    });
console.log("-----------------end get all details-------------------------");
});


// get last process of the component
router.get('/getLastProcessDetails/:componentId/:processStepId', function (req, res) {

console.log("-----------------get all process details-------------------------");
  var retValue = '';
    var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {
    	 
    	 var componentId = req.params.componentId;  
    	 var processStepId = req.params.processStepId;
    	 console.log(componentId);
    	 console.log(processStepId);
    	 
         var  query = ["MATCH (component:Component { component_id: '"+ componentId +"' })-[:HAS]-(mxCredit:MxCreditProcess)-[:HAS]-",
         "(pg:ProcessGroup)-[:HAS]-(sourceProcess:ProcessStep{ID:"+ processStepId +"}) return sourceProcess"].join('\n');
       
        console.log(query);
         graph.query(query, null, function  (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
            console.log(retValue.length);
        });
       
    });
console.log("-----------------end get all details-------------------------");
});

// get last process of the component
router.get('/getAllProcessStatus/:componentId', function (req, res) {

console.log("-----------------get all process details-------------------------");
  var retValue = '';
    var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {
    	 
    	 var componentId = req.params.componentId;  
    	 console.log(componentId);
    	 
         var  query = ["MATCH (component:Component { component_id: '"+ componentId +"' })-[:HAS]-(mxCredit:MxCreditProcess)-[:HAS]-(sw:Swimlane)",
         "MATCH(sw)-[:HAS]-(ps:ProcessStep) return sw,ps order by ps.ID"].join('\n');
       
        console.log(query);
         graph.query(query, null, function  (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
            console.log(retValue.length);
        });
       
    });
console.log("-----------------end get all details-------------------------");
});

router.get('/getProcessStepSchema/:componentId/:processStepId', function (req, res) {

console.log("-----------------get all process details-------------------------");
  var retValue = '';
    var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {
    	 
    	 var componentId = req.params.componentId;  
    	 var processStepId = req.params.processStepId;
    	 console.log(componentId);
    	 console.log(processStepId);
    	 
         var  query = ["MATCH (component:Component { component_id: '"+ componentId +"' })-[:HAS]-(mxCredit:MxCreditProcess)-[:HAS]-",
         "(pg:ProcessGroup)-[:HAS]-(sourceProcess:ProcessStep{ID:"+ processStepId +"})-[:HAS_DATA_FIELD]-> (dataField:DataField) return dataField"].join('\n');
        console.log(query);
         graph.query(query, null, function  (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
            console.log(retValue.length);
        });
       
    });
console.log("-----------------end get all details-------------------------");
});


// creat link node to process 
router.post('/UpdateSchemaWithData', function (req, res) {

console.log("-----------------add hoc-------------------------");
  var retValue = '';
  var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {
    	  
        var query = req.body.query;
        
        graph.query(query, null, function (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
        });
       
    });
console.log("-----------------end creat link node to process -------------------------");
});

router.post('/UpdateProcessStatus', function (req, res) {

console.log("-----------------add hoc-------------------------");
  var retValue = '';
  var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {
    	  
        var query = req.body.query;
        
        graph.query(query, null, function (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
        });
       
    });
console.log("-----------------end creat link node to process -------------------------");
});

module.exports = router;