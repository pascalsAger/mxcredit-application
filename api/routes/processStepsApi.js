var neo4j = require('neo4j-js');
var express = require('express');
var router = express.Router();


var neo4JUrl = 'http://127.0.0.1:7474/db/data/';

router.get('/GetProcessStepsList', function (req, res) {

    console.log("-----------------get all rules-------------------------");
    var retValue = '';
    var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {
    	
        var  query = ["Match(processSchemaTemplateMaster:ProcessSchemaTemplateMaster)-[:HAS]-(ps:ProcessStepTemplate) return ps ORDER BY ps.id"].join('\n');

        //console.log(query);
        graph.query(query, null, function  (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
            console.log(retValue.length);
        });
    });
    console.log("-----------------end get all rules-------------------------");
});


// get all sub components of the component by ID
router.get('/:componentId', function (req, res) {
  var retValue = '';
    var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {
    	    
    	 var componentId = req.params.componentId;    	 
         var  query = ["MATCH (component:Component { component_id:'"+ componentId +"' })-[:HAS]->(mxCredit:MxCreditProcess) return mxCredit"].join('\n');
        
         
         graph.query(query, null, function (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
            console.log(retValue);
        });       
    });
});

// create Process Steps
router.post('/processStep', function (req, res) {

console.log("-----------------add hoc-------------------------");
  var retValue = '';
  var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {
    	  
        var query = req.body.query;
        
        graph.query(query, null, function (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
        });
       
    });
console.log("-----------------end add hoc-------------------------");
});

// Add All Process Steps
router.post('/AddAllProcessSteps', function (req, res) {

console.log("-----------------add hoc-------------------------");
  var retValue = '';
  var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {
    	  
        var query = req.body.query;
        
        graph.query(query, null, function (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
        });
       
    });
console.log("-----------------end add hoc-------------------------");
});

// Add All Process Steps
router.post('/AddProcessData', function (req, res) {

console.log("-----------------add process data-------------------------");
  var retValue = '';
  var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {
    	  
        var query = req.body.query;
        
        graph.query(query, null, function (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
        });
       
    });
console.log("-----------------end add process data -------------------------");
});

// Add All Process Steps
router.post('/AddRulesToSteps', function (req, res) {

console.log("-----------------add hoc-------------------------");
  var retValue = '';
  var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {
    	  
        var query = req.body.query;
        
        graph.query(query, null, function (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
        });
       
    });
console.log("-----------------end add hoc-------------------------");
});


// Add All Process Steps data
router.post('/GetStepDataBySubComName', function (req, res) {

console.log("-----------------add hoc-------------------------");
  var retValue = '';
  var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {
    	  
        var query = req.body.query;
        
        graph.query(query, null, function (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
        });
       
    });
console.log("-----------------end add hoc-------------------------");
});

// Add All Process Steps data
router.post('/AddStepData', function (req, res) {

console.log("-----------------add hoc-------------------------");
  var retValue = '';
  var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {
    	  
        var query = req.body.query;
        
        graph.query(query, null, function (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
        });
       
    });
console.log("-----------------end add hoc-------------------------");
});

router.get('/ProcessGroupsWithChildProcess/:componentId', function (req, res) {

console.log("-----------------get all rules-------------------------");
  var retValue = '';
    var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {
    	 var componentId = req.params.componentId;  
    	 
        /* var  query = ["MATCH (component:Component { component_id:'"+ componentId +"' })-[:HAS] ->(mxCredit:MxCreditProcess)-[:HAS]->",
          "(pg:ProcessGroup)-[:HAS]-(pStep:ProcessStep) return pg  as ProcessGroup,collect(pStep) as ProcessStep  ORDER BY pg.ID"].join('\n');*/
         
         
         var  query = ["MATCH (component:Component { component_id:'"+ componentId +"' })-[:HAS] ",
         "->(mxCredit:MxCreditProcess)-[:HAS]->(pg:ProcessGroup)-[:HAS]-(pStep:ProcessStep)",
         "With  pg  as ProcessGroup,pStep as ProcessStep ORDER BY ProcessStep.ID",
          "return ProcessGroup,collect(ProcessStep) as ProcessStep  ORDER BY ProcessGroup.ID"].join('\n');
         
         //console.log(query);
         graph.query(query, null, function  (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
            console.log(retValue.length);
        });
       
    });
console.log("-----------------end get all rules-------------------------");
});


router.get('/getCriticality/:componentId/:itemId', function (req, res) {

console.log("-----------------get all process details-------------------------");
  var retValue = '';
    var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {
    	 
    	 var componentId = req.params.componentId;  
    	 var itemId = req.params.itemId;
    	 console.log(componentId);
    	 console.log(itemId);
    	 
         var  query = ["MATCH (component:Component { component_id: '"+ componentId +"' })-[:HAS]-(mxCredit:MxCreditProcess)-[:HAS]-",
         "(pg:ProcessGroup)-[:HAS]-(sourceProcess:ProcessStep{ID:3})-",
         "[:HAS]-(sdata:SDATA) where sdata.item_id = '"+ itemId +"' return sdata"].join('\n');
        console.log(query);
         graph.query(query, null, function  (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
            console.log(retValue.length);
        });
       
    });
console.log("-----------------end get all details-------------------------");
});



router.get('/GetProcessStepSchema', function (req, res) {

    console.log("-----------------get all rules-------------------------");
    var retValue = '';
    var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {
      //  var componentId = req.params.componentId;
        /* var  query = ["MATCH (component:Component { component_id:'"+ componentId +"' })-[:HAS] ->(mxCredit:MxCreditProcess)-[:HAS]->",
         "(pg:ProcessGroup)-[:HAS]-(pStep:ProcessStep) return pg  as ProcessGroup,collect(pStep) as ProcessStep  ORDER BY pg.ID"].join('\n');*/
        var  query = ["match (n:SchemaTable) return n.schema "].join('\n');

        //console.log(query);
        graph.query(query, null, function  (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
            console.log(retValue.length);
        });

    });
    console.log("-----------------end get all rules-------------------------");

});


module.exports = router;