var neo4j = require('neo4j-js');
var express = require('express');
var router = express.Router();
var Busboy = require('busboy');
var path = require('path');
var fs = require('fs');
var appDir = path.dirname(require.main.filename);

var neo4JUrl = 'http://127.0.0.1:7474/db/data/';


// get all components of the System

	router.post('/:componentID/:processID', function(req, res) {
      
	console.log(appDir);
	var componentID = req.params.componentID;
	var processID = req.params.processID;

	var busboy = new Busboy({
		headers : req.headers
	}); 

	busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {

		var localFileName = componentID + '_' + processID + '_' + filename;
		console.log(localFileName);
		var saveTo = path.join(appDir, 'app', 'assets', 'documents', localFileName);
		console.log(saveTo);
		file.pipe(fs.createWriteStream(saveTo));
	});

		
	busboy.on('finish', function() {
		console.log("in the file upload finish area. ");
		res.writeHead(200, {
			'Connection' : 'close'
		});
		res.end("That's all folks!");

	}); 

		
	 	req.pipe(busboy);
		
	});

	module.exports = router; 
