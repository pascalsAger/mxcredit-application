var neo4j = require('neo4j-js');
var express = require('express');
var router = express.Router();


var neo4JUrl = 'http://127.0.0.1:7474/db/data/';


// get all components of the System
router.get('/', function (req, res) {

console.log("-----------------get all components-------------------------");
  var retValue = '';
    var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {
    	    
         var  query = ['Match(sub_system:SubSystem),(component:Component),(sub_system)-[:HAS_COMPONENT]->(component) return component order by component.component_id'].join('\n');
        // console.log(query);
         graph.query(query, null, function  (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
            console.log(retValue.length);
        });
       
    });
console.log("-----------------end get all components-------------------------");
});

// get all components of the System
router.get('/componentSubComponent', function (req, res) {

console.log("-----------------get  Sub components-------------------------");
  var retValue = '';
    var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {
    	    
         var  query = ['Match(sub_system:SubSystem),(component:Component),(sub_system)-[:HAS_COMPONENT]->(component)-[:HAS_SUB_COMPONENT]->(SubComponent) return component,collect(SubComponent) AS subComponent'].join('\n');
        
         graph.query(query, null, function  (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
            console.log(error);
        });
       
    });
console.log("-----------------end get  Sub components-------------------------");
});

router.get('/:id', function (req, res) {

console.log("-----------------add hoc-------------------------");
  var retValue = '';
    var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {
    	    
    	   var id = req.params.id;
    	   console.log(id);
         var  query = ['Match(system:System),(component:Component),(system)-[:HAS_COMPONENT]->(component) return component'].join('\n');
         console.log(query);
         graph.query(query, null, function (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
            //console.log(retValue);
        });
       
    });
console.log("-----------------end add hoc-------------------------");
});

// create component
router.post('/', function (req, res) {

console.log("-----------------add hoc-------------------------");
  var retValue = '';
  var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {
    	  
        var query = req.body.query;
        
        graph.query(query, null, function (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
        });
       
    });
console.log("-----------------end add hoc-------------------------");
});


module.exports = router;