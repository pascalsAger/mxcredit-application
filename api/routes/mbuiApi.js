var neo4j = require('neo4j-js');
var express = require('express');
var router = express.Router();


var neo4JUrl = 'http://127.0.0.1:7474/db/data/';




// get all items for a sub component and component
router.get('/GetProcessStepSchema/:processStepID', function (req, res) {
  var retValue = '';
    var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {    	    
    	     	 
         var processID = req.params.processStepID;
         var  query = ["MATCH (:ProcessSchemaTemplateMaster)-[:HAS]-(pst:ProcessStepTemplate{ id:"+processID+" }) return pst"].join('\n');
         graph.query(query, null, function (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
            console.log(retValue);
        });       
    });
});

// update schema template by process step id 
router.post('/', function (req, res) {

console.log("-----------------add hoc-------------------------");
  var retValue = '';
  var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {
    	  
        var query = req.body.query;
        
        graph.query(query, null, function (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
        });
       
    });
console.log("-----------------end add hoc-------------------------");
});





module.exports = router;