var neo4j = require('neo4j-js');
var express = require('express');
var router = express.Router();


var neo4JUrl = 'http://127.0.0.1:7474/db/data/';



// get all sub components of the component by ID
router.get('/GetNotLinkedProcessSteps/:groupID', function (req, res) {
  var retValue = '';
    var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {
    	    
    	 var groupID = req.params.groupID;    	 
         var  query = ["match (p:ProcessSchemaTemplateMaster)-[:HAS]-(step:ProcessStepTemplate) MATCH (g:Group) WHERE ID(g)= "+ groupID +"  MATCH step WHERE NOT (step)-[:HAS]-(g) return step"].join('\n');
        
         console.log(groupID);
         graph.query(query, null, function (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
            console.log(retValue);
        });       
    });
});

// get all sub components of the component by ID
router.get('/GetLinkedProcessSteps/:groupID', function (req, res) {
  var retValue = '';
    var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {
    	    
    	 var groupID = req.params.groupID;    	 
         var  query = ["Match (g:Group) where ID(g) = "+ groupID +" Match (step:ProcessStepTemplate), (g)-[:HAS]-> (step) return step"].join('\n');
        
         console.log(groupID);
         graph.query(query, null, function (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
            console.log(retValue);
        });       
    });
});

// create Process Steps
router.post('/GroupLinkToStep', function (req, res) {

console.log("-----------------add hoc-------------------------");
  var retValue = '';
  var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {
    	  
        var query = req.body.query;
        
        graph.query(query, null, function (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
        });
       
    });
console.log("-----------------end add hoc-------------------------");
});


// create Process Steps
router.post('/RemoveStepGroupAssociation', function (req, res) {

console.log("-----------------add hoc-------------------------");
  var retValue = '';
  var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {
    	  
        var query = req.body.query;
        
        graph.query(query, null, function (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
        });
       
    });
console.log("-----------------end add hoc-------------------------");
});

module.exports = router;