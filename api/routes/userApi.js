
var neo4j = require('neo4j-js');
var express = require('express');
var router = express.Router();
var neo4JUrl = 'http://127.0.0.1:7474/db/data/';


// get all user
router.get('/', function(req, res) {
       var retValue;
    neo4j.connect(neo4JUrl, function(err, graph) {
        if (err)
            throw err;
        
        var query  = ["MATCH (user:User) RETURN user"].join('\n');
        
        graph.query(query, null, function(err, results) {
            if (err) {
                retValue = err;
            } else {
                retValue = results;
            }           
          var returnValue = JSON.stringify(retValue);
          res.json(results);
        //  console.log(returnValue);
        });
    });    
});


// create user
router.post('/', function (req, res) {

console.log("-----------------add user-------------------------");
  var retValue = '';
  var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {
    	  
        var query = req.body.query;
        
        graph.query(query, null, function (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
            console.log(retValue);
        });
       
    });
console.log("-----------------end add user-------------------------");
});

router.post('/RunAddHocQuery', function (req, res) {
var retValue = '';
 var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {
       
        var query = req.body.query;
        //console.log(query);
        graph.query(query, null, function (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });           
        });
       
    });
});

router.post('/GetLoginData', function (req, res) {
  var retValue = '';
 var error = '';
    neo4j.connect(neo4JUrl, function (err, graph) {
     
        var query = req.body.query;
        graph.query(query, null, function (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
        });
       
    });
});

router.get('/:id', function (req, res) {
  var retValue = '';
    	var error = ''; 
    neo4j.connect(neo4JUrl, function (err, graph) {
    	    
    	   var id = req.params.id;
    	   console.log(id);
         var  query = ["Match(user:User) Where ID(user) = "+ id +" return user"].join('\n');
         
         graph.query(query, null, function (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
            //console.log(retValue);
        });
       
    });
});

router.post('/userRelgroup', function (req, res) {
  var retValue = '';
    	var error = ''; 
    neo4j.connect(neo4JUrl, function (err, graph) {    	    
    	 var query = req.body.query;
    	 
         graph.query(query, null, function (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
            //console.log(retValue);
        });
       
    });
});

router.get('/allgroups/:userID', function (req, res) {
  var retValue = '';
    	var error = ''; 
    neo4j.connect(neo4JUrl, function (err, graph) {
    	    
    	   var id = req.params.userID;
    	   console.log(id);
    	  var  query = ["Match(user:User) Where ID(user) = "+ id +"  Match (group:Group)-[:HAS_USER] ->(user) return group"].join('\n');
         // var  query = ["Match(user:User) Where ID(user) = "+ id +" return user"].join('\n');
         
         graph.query(query, null, function (error, results) {
            if (error) {
                error = error;
            }
            else {
                retValue = results;
            }
            res.json({
                responseData: retValue,
                error: error
            });
            //console.log(retValue);
        });
       
    });
});

module.exports = router;