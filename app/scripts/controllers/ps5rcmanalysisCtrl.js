/**
 * Created by rmanagap on 8/26/2016.
 */
mctApp.controller('PS5RCMAnalysisCtrl', function($scope, $rootScope, $location, ProcessStepsService, ComponentShareService, ProcessEngineService, $http, SubComponentService, ItemService,ProcessStepsService,$modal) {
    if ( typeof $rootScope.loggedin == 'undefined' || $rootScope.loggedin == false) {
        $location.path('/login');
        return;
    }

    var loc = $location.path();
    var base_url = window.location.origin;

    $rootScope.leftPanel = false;
    $scope.leftGroupProcessTree = true;

    $scope.selectedComponent = ComponentShareService.getSelectedComponent();
    if ($scope.selectedComponent != false) {
        $scope.componentID = $scope.selectedComponent.data.component_id;
        $scope.componentName = $scope.selectedComponent.data.component_name;
    }

    $scope.currentStep = 5;
    $scope.selectedCriticality;
    // for links and files

    $scope.form = {};

    $scope.componentid = $scope.componentID;
    $scope.processid = $scope.currentStep;

    $scope.selectedRow = null;
    $scope.allSubComponentData;
    $scope.newRow = null;

    $scope.newSubCompDataButton = true;
    $scope.saveSubCompDataButton = false;
    $scope.cancelSubCompDataButton = false;

    $scope.destinationProcessID;

    ProcessEngineService.GetLastProcessStepDetails($scope.selectedComponent.data.component_id, $scope.currentStep).then(function(result) {
        $scope.Process = result;
        //$scope.destinationProcessID = result.data.responseData[0].destinationProcess.data.ID;
        $scope.sourceProcessID = result.data.responseData[0].sourceProcess.id;

        ProcessEngineService.GetCurrentProcessStepSchema($scope.selectedComponent.data.component_id, $scope.currentStep).then(function(result) {
            if (result.data.responseData.length > 0) {
                $scope.schemaStr = result.data.responseData[0].dataField.data.json_schema;

                if ($scope.schemaStr == "none") {

                } else {
                    $scope.schema = angular.fromJson(result.data.responseData[0].dataField.data.json_schema);
                    if ($scope.schema) {
                        $scope.form = $scope.schema[1];
                    }
                }
            }
        });

    });

    $scope.Save = function() {
        console.log($scope.sourceProcessID);

        ProcessEngineService.StateSave($scope.sourceProcessID).then(function(result) {
            //$location.path('/dashboard');
            console.log(result);
        });


        $scope.fieldValues = '{ "1" : ' + JSON.stringify($scope.form) + '}';
        ProcessEngineService.AddDataToProcessStep($scope.componentID, $scope.processid, $scope.fieldValues).then(function(result) {
            //console.log(result);
        });

    };

    $scope.Submit = function() {

        console.log($scope.sourceProcessID);

        ProcessEngineService.FinalDataSubmit($scope.sourceProcessID).then(function(result) {
            //$location.path('/dashboard');
            console.log(result);
        });

        $location.path("processViewTabular");
        //$scope.LoadActiveProcessStep($scope.destinationProcessID);

    };

    $scope.GetNextProcessStep = function(processStepID) {
        var nextStep;
        $http.get('http://localhost:4000/assets/csv/pStepViewMapper.json').success(function(data) {
            $scope.processMaps = data;
            angular.forEach($scope.processMaps, function(processMap) {
                if (processMap.id == processStepID) {
                    nextStep = processMap;
                }
            });
        });
        return nextStep;
    };
    $scope.LoadActiveProcessStep = function(activeProcessID) {
        $http.get(base_url + '/assets/csv/pStepViewMapper.json').success(function(data) {
            $scope.processMaps = data;
            angular.forEach($scope.processMaps, function(processMap) {
                if (processMap.id == activeProcessID) {
                    //console.log(processMap);
                    $location.path(processMap.view);
                }
            });
        });
    };

    // show sub component list of a component in ta dropdownlist
    SubComponentService.GetAllSubComponent($scope.componentID).then(function(result) {
        $scope.subComponents = result.data.responseData;
        //$scope.selectedSubComponent = $scope.subComponents[1];
    });

    $scope.loadItems = function() {
        console.log($scope.selectedSubComponent);
        $scope.items = null;
        if ($scope.selectedSubComponent) {
            ItemService.GetItems($scope.componentID, $scope.selectedSubComponent.SubComponent.data.sub_component_name).then(function(result) {
                //console.log(result);
                $scope.items = result.data.responseData;
                // $scope.dataItem = $scope.items[1];
                //console.log($scope.items);
                $scope.allSubComponentData = null;
            });
        }
    };

    $scope.loadFailureModes = function() {
        //TODO: load failure modes for a particular item
        //console.log($scope.selectedSubComponent);
        //$scope.selectedFM = null;
        //if ($scope.selectedSubComponent) {
        //
        //	ProcessStepsService.Ge
        //	ItemService.GetItems($scope.componentID, $scope.selectedSubComponent.SubComponent.data.sub_component_name).then(function(result) {
        //		//console.log(result);
        //		$scope.items = result.data.responseData;
        //		// $scope.dataItem = $scope.items[1];
        //		//console.log($scope.items);
        //		$scope.allSubComponentData = null;
        //	});
        //}
    };


    $scope.loadRCMData = function() {
        //TODO: load RCM data based on failure modes
        //	console.log($scope.selectedSubComponent);
        console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
        console.log($scope.selectedItem.id);
        console.log($scope.componentID);
        if ($scope.selectedSubComponent) {

            ProcessStepsService.GetAllCriticalityClass($scope.componentID, $scope.selectedItem.id).then(function(result) {
                //console.log(result);
                $scope.CriticalityClass = result.data.responseData;
                //console.log($scope.CriticalityClass);
                /*
                 if (result.data.responseData.length > 0) {
                 //$scope.schema = angular.fromJson(result.data.responseData[0].dataField.data.json_schema);
                 $scope.allSubComponentData = result.data.responseData;
                 }*/
            });

            ProcessStepsService.GetStepAssociatedData( $scope.sourceProcessID, $scope.selectedSubComponent.SubComponent.data.sub_component_name).then(function(result) {
                //console.log(result);
                $scope.allSubComponentData = result.data.responseData;
                /*
                 if (result.data.responseData.length > 0) {
                 //$scope.schema = angular.fromJson(result.data.responseData[0].dataField.data.json_schema);
                 $scope.allSubComponentData = result.data.responseData;
                 }*/
            });
        }

        if ($scope.selectedSubComponent) {
            ItemService.GetItems($scope.componentID, $scope.selectedSubComponent.SubComponent.data.sub_component_name).then(function(result) {
                //console.log(result);
                $scope.items = result.data.responseData;
                // $scope.dataItem = $scope.items[1];
                //console.log($scope.items);
            });
        }
    };

    $scope.addNewRow = function() {

        // $scope.allComponents.push({"name":"", "value":"", "readonly": false});

        $scope.allSubComponentData.push({
            "sdata" : {
                "data" : {
                    "Item_id" : " ",
                    "hidden_evident_failure" : " ",
                    "end_effects" : " ",
                    "mitigation_method" : " ",
                    "mitigation_evaluation" : " ",
                    "fsa_msa_impact" : " ",
                    "mx_concept" : " ",
                    "criticality_class_id" : " ",
                }
            }
        });



        $scope.newRow = $scope.allSubComponentData.length - 1;
        $scope.newSubCompDataButton = false;
        $scope.saveSubCompDataButton = true;
        $scope.cancelSubCompDataButton = true;
    };

    $scope.cancelAllSubComponentData= function (){

        //console.log($scope.allSubComponentData[$scope.allSubComponentData.length -1].component.data);
        $scope.allSubComponentData.splice($scope.allSubComponentData.length -1,1);
        $scope.newRow = null;

        $scope.newSubCompDataButton = true;
        $scope.saveSubCompDataButton = false;
        $scope.cancelSubCompDataButton = false;

    };

    $scope.saveSubCompData= function (){

        console.log("---------------------------------*****************/////////////////////////////");
        console.log($scope.selectedItem.id);
        console.log($scope.selectedCriticality.id);

        console.log($scope.allSubComponentData[$scope.allSubComponentData.length -1].sdata.data);


        if ($scope.selectedSubComponent) {
            if ($scope.selectedItem.id) {

                ProcessStepsService.AddRcmData($scope.sourceProcessID,$scope.selectedSubComponent.SubComponent.data.sub_component_name,$scope.selectedItem.id,$scope.allSubComponentData[$scope.allSubComponentData.length - 1].sdata.data,$scope.selectedCriticality.id).then(function(result) {
                    console.log(result);
                });
            }
        }

        $scope.newSubCompDataButton = true;
        $scope.saveSubCompDataButton = false;
        $scope.cancelSubCompDataButton = false;

        $scope.newRow = null;


    };
    /*
     $scope.itemChanged = function() {
     console.log($scope.selectedItem.id);
     };
     */

    $scope.units = [
        {'id': 10, 'label': 'test1'},
        {'id': 27, 'label': 'test2'},
        {'id': 39, 'label': 'test3'},
    ];

    $scope.data = {
        'id': 1,
        'unit': 27
    };
    $scope.selectedItem = {
        'id': 1,
        'unit': 27
    };
    $scope.selectedCriticality = {
        'id': 1,
        'unit': 28
    };

    $scope.unitChanged = function() {
        console.log($scope.data.unit);
    };

    // modal for information

    //$scope.processStep = {"name": "System FMECA", "type": "", "description": "Selection of A/C system from a given aircraft breakdown structure (ATA/SBC)"};
    var multilineText =[];
    multilineText.push("Aggregation of component FMEAs into a system FMEA.");
    multilineText.push("Identify the failures, modes, effects and criticality. (Component Design Authority)");
    $scope.processStep = {
        "name": "System FMEA",
        "type": "",
        "description": multilineText
    };
    var ModalInstanceCtrl = function ($scope, $modalInstance,processStep) {
        $scope.processStep = processStep;
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    };

    $scope.fms = ['','FM1','FM2']
    $scope.cclevels = ['','CC5','CC6']

    $scope.open = function () {
        var modalInstance = $modal.open({
            templateUrl: 'views/modals/info_process_step.html',
            controller: ModalInstanceCtrl,
            resolve: {
                processStep: function () {
                    return $scope.processStep;
                }
            }
        });
        modalInstance.result.then(function () {
            // $scope.LoadGroups();
        }, function () {
        });
    };

    // end of modal

});
