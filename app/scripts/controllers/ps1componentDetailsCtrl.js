mctApp.controller('PS1ComponentDetailsCtrl', function($scope, $rootScope, $location, ProcessStepsService, ComponentShareService, ProcessEngineService, $http,SubComponentService,FormService,$modal,ItemService) {
	if ( typeof $rootScope.loggedin == 'undefined' || $rootScope.loggedin == false) {
		$location.path('/login');
		return;
	}
    $rootScope.setProcessStepActive();
    var loc = $location.path();
    var base_url = window.location.origin;
	$rootScope.leftPanel = false;
	$scope.leftGroupProcessTree = true;
    $scope.selectedComponent = ComponentShareService.getSelectedComponent();
    if ($scope.selectedComponent != false) {
		$scope.componentID = $scope.selectedComponent.data.component_id;
		$scope.componentName = $scope.selectedComponent.data.component_name;
	}
	
	$scope.form = {};
	/*
	FormService.GetProcessSchema().then(function(result) {
		//$scope.schema = result.data.responseData[0].schema;
		$scope.schema = angular.fromJson(result.responseData[0].schema);
		console.log(angular.fromJson(result.responseData[0].schema));
		$scope.form = $scope.schema[1];
	});
	*/
	/*
	FormService.form(1).then(function(form) {
		$scope.form = form;
	});
	*/
   //$scope.processInfo = {};	
   $scope.currentStep = 1;
   $scope.componentid = $scope.componentID;
   $scope.processid = $scope.currentStep;

   $scope.subComponents;
   
  
	SubComponentService.GetAllSubComponent($scope.componentID).then(function(result) {
		$scope.subComponents = result.data.responseData;

		angular.forEach($scope.subComponents, function(obj) {
			ItemService.GetItems($scope.componentID, obj.SubComponent.data.sub_component_name).then(function(result) {
				console.log(result);
				obj.items = result.data.responseData;
				//console.log($scope.items);
			});
		});

	}); 

   
	
	$scope.destinationProcessID;
	
	
	ProcessEngineService.GetCurrentProcessStepDetails($scope.selectedComponent.data.component_id, $scope.currentStep).then(function(result) {
		$scope.Process = result;
		
		$scope.destinationProcessID = result.data.responseData[0].destinationProcess.data.ID;
		$scope.sourceProcessID = result.data.responseData[0].sourceProcess.id;
		
		ProcessEngineService.GetCurrentProcessStepSchema($scope.selectedComponent.data.component_id, $scope.currentStep).then(function(result) {
			if(result.data.responseData.length > 0)
			{
				$scope.schemaStr = result.data.responseData[0].dataField.data.json_schema;
				
				if ($scope.schemaStr == "none") {

				} else {
					$scope.schema = angular.fromJson(result.data.responseData[0].dataField.data.json_schema);
					if ($scope.schema) {
						$scope.form = $scope.schema[1];
						console.log($scope.form);
					}
				}						
			}
		});
	}); 

    $scope.Save = function() {
        
         console.log($scope.sourceProcessID);
        
        ProcessEngineService.StateSave($scope.sourceProcessID).then(function(result) {
			//$location.path('/dashboard');
			console.log(result);
		});
        
        
		$scope.fieldValues = '{ "1" : ' + JSON.stringify($scope.form) + '}';
				
		ProcessEngineService.AddDataToProcessStep($scope.componentID,$scope.processid,$scope.fieldValues).then(function(result) {
			console.log(result);
		});
       
	};
    
    
	$scope.Submit = function() {
        console.log($scope.sourceProcessID);
        
        ProcessEngineService.FinalDataSubmit($scope.sourceProcessID).then(function(result) {
			//$location.path('/dashboard');
			console.log(result);
		});
        
		$scope.LoadActiveProcessStep($scope.destinationProcessID);

	}; 
    $scope.files;
	
	$scope.GetNextProcessStep = function( processStepID) {
       var nextStep;
		$http.get('http://localhost:4000/assets/csv/pStepViewMapper.json').success(function(data) {
			$scope.processMaps = data;
			angular.forEach($scope.processMaps, function(processMap) {
				if(processMap.id == processStepID)
				{
					nextStep = processMap;
				}				
			});
		});
		return nextStep;
	};
	$scope.LoadActiveProcessStep = function(activeProcessID) {
		$http.get(base_url+'/assets/csv/pStepViewMapper.json').success(function(data) {
			$scope.processMaps = data;
			angular.forEach($scope.processMaps, function(processMap) {
				if(processMap.id == activeProcessID)
				{
					//console.log(processMap);
					$location.path(processMap.view);	
				}				
			});
		});
	};
	
	/*if (angular.isDefined($scope.selectedComponent.data.component_id)) {
		ProcessEngineService.GetAllSubActiveProcess($scope.selectedComponent.data.component_id).then(function(result) {
			$scope.activeProcess = result;
			if ($scope.activeProcess.data.responseData.length > 0) {
				$scope.LoadActiveProcessStep($scope.activeProcess.data.responseData[0].sourceProcess.data.ID);
			}
		});
	}*/
   /*
	$scope.LoadActiveProcessStep = function(activeProcessID) {
		$http.get('http://localhost:4000/assets/csv/pStepViewMapper.json').success(function(data) {
			$scope.processMaps = data;
			angular.forEach($scope.processMaps, function(processMap) {
				if(processMap.id == activeProcessID)
				{
					//console.log(processMap);
					$location.path('subComponent');	
				}				
			});
		});
	};
	*/
	
	
	// modal for information
	
	var multilineText =[];
	multilineText.push("ISHM requirements already considered for A/C concept (Airbus Dare M2-M3; ADS CADE: REQ/DES Phase)");
	multilineText.push("Selection of A/C system from a given aircraft breakdown structure (ATA/SBC)");
	multilineText.push("Component FMECA is provided by supplier - test");
	$scope.processStep = {
		"name": "Component Details",
		"type": "",
		"description": multilineText
	};
	
	   var ModalInstanceCtrl = function ($scope, $modalInstance,processStep) {
        $scope.processStep = processStep;
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    };

    $scope.open = function () {
        var modalInstance = $modal.open({
            templateUrl: 'views/modals/info_process_step.html',
            controller: ModalInstanceCtrl,
            resolve: {
                processStep: function () {
                    return $scope.processStep;
                }
            }
        });
        modalInstance.result.then(function () {
           // $scope.LoadGroups();
        }, function () {
        });
    };
	
	// end of modal
});
