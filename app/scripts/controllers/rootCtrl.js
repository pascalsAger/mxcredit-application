mctApp.controller('RootCtrl', function ($scope, $http, UserService, Base64, $rootScope, $location,componentService,SubComponentService) {
    $scope.logout = function () {
        //Session.clear();
        $location.path('/login');
    };
    $scope.changeView = function (view) {
        $location.path(view);
    };
   $scope.allComponentsSubComponent;
   $scope.subComponents;
 

	$scope.LoadComponentSubComponents = function() {

		componentService.GetAllComponentSubComponent().then(function(result) {
			$scope.allComponentsSubComponent = result.data.responseData;
			$scope.LoadTree($scope.allComponentsSubComponent);

		});

		/*
		 $scope.parent = [];
		 $scope.name = {};
		 $scope.name.firstname ="mushfiq";
		 $scope.name.lastname = "rahman";
		 $scope.name.children = [];
		 $scope.subcomponent = {};
		 $scope.subcomponent.firstname ="angshu";
		 $scope.name.children = $scope.name.children.concat($scope.subcomponent);
		 $scope.parent = $scope.parent.concat($scope.name);
		 $scope.dataForTheTree =  $scope.parent;
		 console.log(angular.toJson($scope.parent));
		 */
	};

	
	$scope.LoadTree = function(compSubcomp)
	{
		
	    $scope.parent = [];
	    
		for (var i = 0; i < compSubcomp.length; i++) {
			
			$scope.treeNode = {};
			$scope.treeNode.name = compSubcomp[i].component.data.component_name;
			$scope.treeNode.children = []; 
            if(compSubcomp[i].subComponent.length > 0)
            {
            	for (var j = 0; j < compSubcomp[i].subComponent.length; j++) 
            	{
            		$scope.subcomponent = {};
            		$scope.subcomponent.name = compSubcomp[i].subComponent[j].data.sub_component_name;
            		$scope.treeNode.children = $scope.treeNode.children.concat($scope.subcomponent);
            	}
            }
            $scope.parent = $scope.parent.concat($scope.treeNode);
                      
		}
		$scope.dataForTheTree =  $scope.parent;  
        
	};
	
	

	
		$scope.treeOptions = {
	    nodeChildren: "children",
	    dirSelectable: true
	    
		}
	
	//$scope.dataForTheTree = $scope.allComponentsSubComponent;
	
	/*
	$scope.dataForTheTree =
	[
	    { "name" : "Joe", "age" : "21", "children" : [
	        { "name" : "Smith", "age" : "42", "children" : [] },
	        { "name" : "Gary", "age" : "21", "children" : [
	            { "name" : "Jenifer", "age" : "23", "children" : [
	                { "name" : "Dani", "age" : "32", "children" : [] },
	                { "name" : "Max", "age" : "34", "children" : [] }
	            ]}
	        ]}
	    ]},
	    { "name" : "Albert", "age" : "33", "children" : [] },
	    { "name" : "Ron", "age" : "29", "children" : [] }
	];
	*/
    $scope.showSelected = function (node) {
        console.log(node);
      };
     // $scope.processbuttonselected = "process-button-selected";
  $rootScope.setProcessStepActive = function() {
        $scope.processbuttonselected = "process-button-selected";
        $scope.processViewselected  = "";
    };
    $rootScope.setProcessViewActive = function() {
        $scope.processbuttonselected = "";
        $scope.processViewselected = "process-button-selected";
    };
  
});