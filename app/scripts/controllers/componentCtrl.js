mctApp.controller('ComponentCtrl', function($scope, $rootScope, $location, componentService) {
	if ( typeof $rootScope.loggedin == 'undefined' || $rootScope.loggedin == false) {
		$location.path('/login');
		return;
	}
	
	$scope.system = 'Flight Control System';
	$scope.subSystem = 'Aileron Tab';

	$scope.AddNewComponent = function() {
		//console.log("clicked");
		componentService.AddNewComponent($scope).then(function(result) {
			$location.path('/dashboard');
		});
	};
}); 