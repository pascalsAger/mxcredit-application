mctApp.controller('ItemCtrl', function($scope, $rootScope, $location, ItemService, ComponentShareService, SubComponentShareService,$modal) {
	if ( typeof $rootScope.loggedin == 'undefined' || $rootScope.loggedin == false) {
		$location.path('/login');
		return;
	}
   
   $scope.selectedComponent = ComponentShareService.getSelectedComponent(); 
   $scope.selectedSubComponent = SubComponentShareService.getSelectedSubComponent();
   
   
	if ($scope.selectedComponent) {
		$scope.componentID = $scope.selectedComponent.data.component_id;
		$scope.componentName = $scope.selectedComponent.data.component_name;
		$scope.subComponentName = $scope.selectedSubComponent.data.sub_component_name;

		$scope.items;
		$scope.newItemButton = true;
		$scope.saveItemButton = false;
		$scope.cancelItemButton = false;

		$scope.LoadItems = function() {
			ItemService.GetItems($scope.componentID, $scope.subComponentName).then(function(result) {
				console.log(result);
				$scope.items = result.data.responseData;
				//console.log($scope.items);
			});

		};
	} else {
		$location.path('/dashboard');
	}

   
   
   
	$scope.AddNewItem = function() {
		ItemService.AddNewItem($scope.componentID,$scope.subComponentName,$scope).then(function(result) {
			console.log(result);
			$location.path('items');    
		});
	};
	
    var ModalInstanceCtrl = function ($scope, $modalInstance,itemID,ItemService) {
    	$scope.type = "Item";
        $scope.name = "Item";
        $scope.ok = function () {
        	
        	ItemService.DeleteItemById(itemID).then(function(result) {
			console.log(result);
			  $modalInstance.close(); 
		});
        };
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    };

    /**
     * Open Modal
     */
    $scope.open = function (itemID) {
        var modalInstance = $modal.open({
            templateUrl: 'views/modals/delete_modal.html',
            controller: ModalInstanceCtrl,
            resolve: {
                itemID: function () {
                    return itemID;
                }
            }
        });
        modalInstance.result.then(function () {
          $scope.LoadItems();
       }, function () {});
    };
	
	/* end of modal */
		

	$scope.addNewRow = function() {

		$scope.items.push({
			"Item" : {
				"data" : {
					"item_name" : "",
					"part_no" : "",
					"sbc" : "",
					"supplier_details" : "",
					"supplier_name" : "",
				}
			}
		});

		$scope.newRow = $scope.items.length - 1;

		$scope.newItemButton = false;
		$scope.saveItemButton = true;
		$scope.cancelItemButton = true;
	};
    
 
	$scope.saveItem = function() {

		console.log($scope.items[$scope.items.length - 1]);
       /*
		SubComponentService.AddNewSubComponent($scope.subComponents[$scope.subComponents.length - 1].SubComponent.data, $scope.componentID).then(function(result) {
			SubComponentService.GetAllSubComponent($scope.componentID).then(function(result) {
				$scope.subComponents = result.data.responseData;
			});
		});
       */
		ItemService.AddNewItem($scope.componentID, $scope.subComponentName, $scope.items[$scope.items.length - 1]).then(function(result) {
			ItemService.GetItems($scope.componentID, $scope.subComponentName).then(function(result) {
				console.log(result);
				$scope.items = result.data.responseData;
				//console.log($scope.items);
			});
		});

		$scope.newItemButton = true;
		$scope.saveItemButton = false;
		$scope.cancelItemButton = false;
		$scope.newRow = null;
	}; 
	
	$scope.cancel= function (){
		
		//console.log($scope.allComponents[$scope.allComponents.length -1].component.data);
		$scope.items.splice($scope.items.length -1,1);
		
		$scope.newRow = null;
		
		$scope.newItemButton = true;
        $scope.saveItemButton = false;
        $scope.cancelItemButton = false;
	};


}); 