mctApp.controller('ProcessStepsCtrl', function($scope, $rootScope, $location, ProcessStepsService, ComponentShareService, ProcessEngineService, $http, FormService, ProcessStepsService) {
	if ( typeof $rootScope.loggedin == 'undefined' || $rootScope.loggedin == false) {
		$location.path('/login');
		return;
	}
    var base_url = window.location.origin;
    $scope.selectedComponent = ComponentShareService.getSelectedComponent();
    
   
	if ($scope.selectedComponent) {
		if (angular.isDefined($scope.selectedComponent.data.component_id)) {
			ProcessEngineService.GetAllSubActiveProcess($scope.selectedComponent.data.component_id).then(function(result) {
				$scope.activeProcess = result;
				if ($scope.activeProcess.data.responseData.length > 0) {
					$scope.LoadActiveProcessStep($scope.activeProcess.data.responseData[0].sourceProcess.data.ID);
				}
			});
		}
	} else {
		$location.path('/dashboard');
	}


	$scope.LoadActiveProcessStep = function(activeProcessID) {
		$http.get(base_url + '/assets/csv/pStepViewMapper.json').success(function(data) {
			$scope.processMaps = data;
			angular.forEach($scope.processMaps, function(processMap) {
				if (processMap.id == activeProcessID) {
					//console.log(processMap);
					$location.path(processMap.view);
				}
			});
		});
	}; 

    

	//$rootScope.leftPanel = false;
	//$scope.leftGroupProcessTree = true;
	/*
	var base_url = window.location.origin;
	$scope.AddValue = function() {
		$scope.value = $scope.data_source;
	};
	$rootScope.leftPanel = false;

	$scope.leftGroupProcessTree = true;
	$scope.selectedComponent = ComponentShareService.getSelectedComponent();
    //$scope.somedata = "dddddddddddddddddddddddddddddddddddd";
	$scope.form = {};
	FormService.GetProcessSchema().then(function(result) {
		//$scope.schema = result.data.responseData[0].schema;
		$scope.schema = angular.fromJson(result.responseData[0].schema);
		console.log(angular.fromJson(result.responseData[0].schema));
		$scope.form = $scope.schema[1];
	});
	*/

    /*
	// read form with given id
	FormService.form(1).then(function(form) {
		$scope.form = form;
	});
	*/
    /*
	$scope.testSubmit = function() {

		$scope.fieldValues = '{ "1" : ' + JSON.stringify($scope.form) + '}';
		//$scope.form.form_fields;

		FormService.AddDataToProcessStep($scope.fieldValues).then(function(result) {
			$location.path('/dashboard');
		});

	};
	*/
	/*
	 if (angular.isDefined($scope.selectedComponent.data.component_id)) {
	 ProcessEngineService.GetAllSubActiveProcess($scope.selectedComponent.data.component_id).then(function(result) {
	 $scope.activeProcess = result;
	 if ($scope.activeProcess.data.responseData.length > 0) {
	 $scope.LoadActiveProcessStep($scope.activeProcess.data.responseData[0].sourceProcess.data.ID);
	 }
	 });
	 }

	 $scope.LoadActiveProcessStep = function(activeProcessID) {
	 $http.get(base_url+'/assets/csv/pStepViewMapper.json').success(function(data) {
	 $scope.processMaps = data;
	 angular.forEach($scope.processMaps, function(processMap) {
	 if(processMap.id == activeProcessID)
	 {
	 //console.log(processMap);
	 $location.path(processMap.view);
	 }
	 });
	 });
	 };
	 */
});
