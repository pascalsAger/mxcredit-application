mctApp.controller('ProcessViewTabularCtrl', function($scope, $rootScope, $location,ProcessViewService,ComponentShareService) {
	if ( typeof $rootScope.loggedin == 'undefined' || $rootScope.loggedin == false) {
		$location.path('/login');
		return;
	}

	$rootScope.leftPanel = false;
	$rootScope.setProcessViewActive();
	$scope.leftGroupProcessTree = true;
    $scope.currentStep = 1;
	$scope.AddValue = function() {
		$scope.value = $scope.data_source;
	};
	
    var base_url = window.location.origin;
   // $scope.componentID = "CC1";
   // $scope.row = "row-color";
    
   $scope.selectedComponent = ComponentShareService.getSelectedComponent();
    

	if ($scope.selectedComponent) {
		if (angular.isDefined($scope.selectedComponent.data.component_id)) {
			ProcessViewService.GetAllStepsStatus($scope.selectedComponent.data.component_id).then(function(result) {
				$scope.allStepsDetails = result.data.responseData;

				angular.forEach($scope.allStepsDetails, function(obj) {

					if (obj.ps.data.isDataSubmitted == "TRUE") {
						obj.rowColor = "row-color-green";
					} else {
						if (obj.ps.data.isActive == "TRUE") {
							obj.rowColor = "row-color-orange";
						} else {
							obj.rowColor = "row-color-white";
						}
					}
				});
			});
		}
	} else {
		$location.path('/dashboard');
	}

  
	

  	
  	
  	//$scope.processSVG = base_url+"/assets/csv/Process_View_Sample_171215.svg";
	/*
   $http.get(base_url + 'assets/js/angularxml/courseDef.xml').then(function(response) {
    var chapters = [];

    var courseDef = x2js.xml_str2json(response.data);
    $scope.chaptersObj = courseDef.course.navigation.chapter;

    
    var numOfChapters = $scope.chaptersObj.length;
    for (var i = 0; i < numOfChapters; i++) {
      chapters.push({
        name: $scope.chaptersObj[i].name,
        number: $scope.chaptersObj[i]._number
      });
    }

    $scope.chapterNames = chapters;
  });
  */
  
});
