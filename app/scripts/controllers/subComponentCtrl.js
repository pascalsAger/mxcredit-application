mctApp.controller('SubComponentCtrl', function($scope, $rootScope, $location,SubComponentService, ComponentShareService,SubComponentShareService) {
	if ( typeof $rootScope.loggedin == 'undefined' || $rootScope.loggedin == false) {
		$location.path('/login');
		return;
	}
	
	$scope.newSubComponentButton = true;
    $scope.saveSubComponentButton = false;
    $scope.cancelSubComponentButton = false;
   
   $scope.selectedComponent = ComponentShareService.getSelectedComponent();
   
	
	if ($scope.selectedComponent) {
		$scope.componentID = $scope.selectedComponent.data.component_id;
		$scope.componentName = $scope.selectedComponent.data.component_name;

		$scope.subComponents;

		SubComponentService.GetAllSubComponent($scope.componentID).then(function(result) {
			$scope.subComponents = result.data.responseData;
		});

	} else {

		$location.path('/dashboard');
	}

	$scope.AddNewSubComponent = function() {
		SubComponentService.AddNewSubComponent($scope,$scope.componentID).then(function(result) {
			$location.path('/subComponent');
		});
	};
	
	$scope.showAllItems = function (subComponent) {  
		  	if ($scope.selectedComponent != false) {    	
		    SubComponentShareService.addSelectedSubComponent(subComponent);
		    $location.path('items');    
		   }
    };
    
    $scope.addNewRow = function() {
    	
		$scope.subComponents.push({
			"SubComponent" : {
				"data" : {
					"part_no" : "",
					"sbc" : "",
					"sub_component_name" : "",
					"supplier_details" : "",
					"supplier_name" : "",
				}
			}
		}); 
        
       $scope.newRow = $scope.subComponents.length -1;     
     
		$scope.newSubComponentButton = false;
		$scope.saveSubComponentButton = true;
		$scope.cancelSubComponentButton = true; 

	}; 
	
	$scope.saveSubComponent= function (){
		
		console.log($scope.subComponents[$scope.subComponents.length -1]);		
		
		SubComponentService.AddNewSubComponent($scope.subComponents[$scope.subComponents.length -1].SubComponent.data, $scope.componentID).then(function(result) {
			SubComponentService.GetAllSubComponent($scope.componentID).then(function(result) {
				$scope.subComponents = result.data.responseData;
			});
		}); 
		
        $scope.newSubComponentButton = true;
		$scope.saveSubComponentButton = false;
		$scope.cancelSubComponentButton = false; 
		$scope.newRow = null;		
	};
	
	$scope.cancel= function (){
		
		//console.log($scope.allComponents[$scope.allComponents.length -1].component.data);
		$scope.subComponents.splice($scope.subComponents.length -1,1);
		$scope.newRow = null;
		$scope.newSubComponentButton = true;
        $scope.saveSubComponentButton = false;
        $scope.cancelSubComponentButton = false;
	};
	
}); 