mctApp.controller('UsersCtrl', function ($scope, $http, UserService, $rootScope, $location, $modal,GroupService) {
    if (typeof  $rootScope.loggedin == 'undefined' || $rootScope.loggedin == false) {
        $location.path('/login');
        return;
    }
    //$scope.users = UserService.get();
   $rootScope.leftPanel = false; 
   $scope.users;
    $scope.newUser =  {
        "display_name": "",
        "first_name": "",
        "last_name": "",
        "email": "",
        "password": ""
    };
    
    $scope.createUser = function (newUser) {
        var user = new UserService(newUser);
        user.id= newUser.id;
        user.firstName= newUser.firstName;
        user.lastName= newUser.lastName;
        user.email= newUser.email;
        user.password= newUser.password;

        user.$save(function (u, putResponseHeaders) {
            $scope.users.data.push(u);
        });
    };   
      $scope.LoadUsers= function() {
	        UserService.GetAllUser().then(function(result) {
			
			$scope.users= result.data;
			
		});	
	};
   
  
    var ModalInstanceCtrl = function ($scope, $modalInstance, newUser, UserService) {
        $scope.newUser = newUser;
        $scope.submit = function () {
            UserService.AddNewUser($scope.newUser).then(function(result) {
			console.log(result);
		     $modalInstance.close(); 
		   });
        };
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    };

    $scope.open = function () {
        var modalInstance = $modal.open({
            templateUrl: 'views/modals/createUser.html',
            controller: ModalInstanceCtrl,
            resolve: {
                newUser: function () {
                    return $scope.newUser;
                }
            }
        });
        modalInstance.result.then(function (newUser) {
           $scope.LoadUsers();
           console.log("then then athen");
        }, function () {
        });
    };


    $scope.removeUser = function(user)
    {
        UserService.delete({"user": user.id}, function (data) {
            $scope.users = UserService.get();
        });
    };
    
    $scope.LoadUserDetail= function(userID) {
    	    $scope.selectedUserID = userID;
    	    console.log("selected user id" + userID);
	       
		UserService.GetUserById(userID).then(function(result) {
			if (result.data.responseData.length > 0) {
				$scope.user = result.data.responseData[0].user;
				$scope.displayName = $scope.user.data.display_name;
				$scope.firstName = $scope.user.data.first_name;
				$scope.lastName = $scope.user.data.last_name;
				$scope.email = $scope.user.data.email;
				$scope.password = $scope.user.data.password;
			}

			$scope.LoadUserGroups(userID);
		}); 

	
	};
	
	
	$scope.LoadUserGroups = function(userID) {

		UserService.GetUserGroups(userID).then(function(result) {
			$scope.userGroups = result.data.responseData;
		});
	};

    
     var ModalInstanceUserGroupCtrl = function ($scope, $modalInstance,groups,userID, UserService) {
        $scope.groups = groups;
        $scope.userID = userID;
        
        $scope.selectGroup = function (group) {
         
         console.log(group.group.id);
         console.log("doubled clicked user id" + userID);
         
         UserService.AddUserRelGroup(userID,group.group.id).then(function(result) {
			console.log(result);
		     $modalInstance.close(); 
		   });  
		   
        };
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    };
    
    $scope.openGroups = function (groups,userID) {
        var modalInstance = $modal.open({
            templateUrl: 'views/modals/userGroups.html',
            controller: ModalInstanceUserGroupCtrl,
            resolve: {
                groups: function () {
                    return groups;
                },
                userID: function () {
                    return userID;
                }
            }            
        });
        modalInstance.result.then(function () {
           $scope.LoadUsers();
           console.log("then then athen");
        }, function () {
        });
    };
    
     $scope.LoadGroups= function() 
	 {
	        GroupService.GetAllGroups().then(function(result) {
			
			$scope.groups = result.data.responseData;
			$scope.openGroups($scope.groups,$scope.selectedUserID);
		});	
	};
	
    $scope.query = "";
});