'use strict';
//mctApp.controller('SubComponentCtrl', function($scope, $rootScope, $location,SubComponentService, ComponentShareService,SubComponentShareService) {
var ViewCtrl = mctApp.controller('ViewCtrl', function ($scope, FormService, $routeParams) {
    $scope.form = {};
	// read form with given id
	FormService.form($routeParams.id).then(function(form) {
		$scope.form = form;
	});
	
	 $scope.testSubmit = function () {
        $scope.fieldValues =  $scope.form.form_fields;
    };
    
});
