 mctApp.controller('DashboardCtrl', function ($scope, $rootScope, $location, componentService, ComponentShareService,ProcessStepsService) {
    if (typeof  $rootScope.loggedin == 'undefined' || $rootScope.loggedin == false) {
        $location.path('/login');
        return;
    }
    //console.log($rootScope.userID);
    $scope.selectedRow = null; 
    $rootScope.leftPanel = true;
    $scope.ShowCreateProcessSteps = false;
    $scope.ShowRunProcessSteps = false;
    $scope.allComponents;
    $scope.newRow = null;  
    
     
    $scope.newComponentButton = true;
    $scope.saveComponentButton = false;
    $scope.cancelComponentButton = false;
    
    $scope.ac_system = "Flight Control System";
    $scope.ac_sub_system = "Aileron Tab";
    
    $scope.CreateNewComponent = function () 
    {
              componentService.CreateNewComponent().then(function(result){
		           // console.log(result.data);		          		            
                });
    };
    
    $scope.showSubComponents = function (component) 
    {
    	    	
		    ComponentShareService.addSelectedComponent(component);
		    $location.path('subComponent');    
    };
    
     $scope.LoadComponents = function() 
	 {
	          componentService.GetAllComponent()
                .then(function(result){
		           $scope.allComponents = result.data.responseData;
                });           
	 };
	 
	 $scope.CheckMxCreditProcessStatus = function (component,index) 
	 {
	 	    
		if ($scope.saveComponentButton == false) {
			console.log(component.component.data.component_id);
			ComponentShareService.addSelectedComponent(component.component);
			$scope.selectedRow = index;
			$scope.SelectedComponentID = component.component.data.component_id;

			ProcessStepsService.IsMxCreditProcessAvailable(component.component.data.component_id).then(function(result) {
				$scope.ProcessStep = result;
				if ($scope.ProcessStep.data.responseData.length > 0) {

					$scope.ShowCreateProcessSteps = false;
					$scope.ShowRunProcessSteps = true;
				} else {
					//ComponentShareService.removeSelectedComponent();
					$scope.ShowCreateProcessSteps = true;
					$scope.ShowRunProcessSteps = false;
				}
			});
		}  
	 
    };
    

	$scope.CreateProcessSteps = function() {
		console.log($scope.SelectedComponentID);

		ProcessStepsService.CreateMxCreditProcess($scope.SelectedComponentID).then(function(result) {
			console.log("inside create process step");
			
			ProcessStepsService.AddAllProcessSteps($scope.SelectedComponentID).then(function(result) {
				  console.log(" inside add process steps");
				ProcessStepsService.AddRulesToSteps($scope.SelectedComponentID).then(function(result) {
						console.log(" inside add rules");
			        });
				
			});
		});
	};
	
	
	$scope.datagridtextbox = "datagrid-textbox";
	$scope.addNewRow = function() {

		// $scope.allComponents.push({"name":"", "value":"", "readonly": false});
		
		$scope.allComponents.push({
			"component" : {
				"data" : {
					"ata_chapter" : "",
					"component_id" : "",
					"component_name" : "",
					"partNo" : "",
					"sbc" : "",
					"supplier_name" : "",     <!--Advith-->
					"supplier_details" : "",
					}
			}
		}); 
     $scope.newRow = $scope.allComponents.length -1;
     $scope.newComponentButton = false;
     $scope.saveComponentButton = true;
     $scope.cancelComponentButton = true;
	}; 
	
	$scope.SaveComponent= function (){
		
		console.log($scope.allComponents[$scope.allComponents.length -1].component.data);
		componentService.AddNewComponent($scope.allComponents[$scope.allComponents.length -1].component.data).then(function(result) {
			componentService.GetAllComponent()
                .then(function(result){
		           $scope.allComponents = result.data.responseData;
                });  
		});
		
		$scope.newComponentButton = true;
        $scope.saveComponentButton = false;
        $scope.cancelComponentButton = false;
        
        $scope.newRow = null;
	};
	
	$scope.cancel= function (){
		
		console.log($scope.allComponents[$scope.allComponents.length -1].component.data);
		$scope.allComponents.splice($scope.allComponents.length -1,1);
		$scope.newRow = null;
		$scope.newComponentButton = true;
        $scope.saveComponentButton = false;
        $scope.cancelComponentButton = false;
	};
   
});