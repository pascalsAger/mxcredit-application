mctApp.controller('processTreeCtrl', function($scope, $rootScope, $location,$http, GroupService, $modal, ProcessStepsService, ComponentShareService,UserService,PStepGroupServer) {
	if ( typeof $rootScope.loggedin == 'undefined' || $rootScope.loggedin == false) {
		$location.path('/login');
		return;
	}
   
    
  // console.log("**************************************");
  // console.log($rootScope.userID);
   
  
	
	$scope.sort = function() {
		//console.log("yahooooooooooooooooooooooooooooooooooooooooooooooooooooo");

		UserService.GetUserGroups($rootScope.userID).then(function(result) {
			$scope.userGroups = result.data.responseData;
			console.log($scope.userGroups);
			if ($scope.userGroups.length > 0) {
				PStepGroupServer.GetLinkedSteps($scope.userGroups[0].group.id).then(function(result) {
					$scope.linkedProcessSteps = result.data.responseData;
					console.log($scope.linkedProcessSteps);
					$scope.LoadAssociatedProcessGroupsWithProcess($scope.linkedProcessSteps);
				});
			}

		});
		//console.log("yahooooooooooooooooooooooooooooooooooooooooooooooooooooo");
	};

   
	$scope.allGroupsWithProcess;
	$scope.ProcessSteps;
	$scope.parentID;
	$scope.childID;
	var base_url = window.location.origin;
	
	$scope.LoadProcessGroupsWithProcess = function() {
		var componentID;
		$scope.selectedComponent = ComponentShareService.getSelectedComponent();
		if ($scope.selectedComponent != false) {
			componentID = $scope.selectedComponent.data.component_id;
			console.log("selected component from components view is .....");
			console.log(componentID);
			
			ProcessStepsService.GetAllProcessGroupWithProcessSteps(componentID).then(function(result) {
				$scope.allGroupsWithProcess = result.data.responseData;
				
				if ($scope.allGroupsWithProcess.length > 0) {
					$scope.LoadTree($scope.allGroupsWithProcess);
				}

			});
		}
	};

	$scope.LoadTree = function(allGroupsWithProcess) {

		$scope.parent = [];

		for (var i = 0; i < allGroupsWithProcess.length; i++) {

			$scope.treeNode = {};
			$scope.treeNode.name = allGroupsWithProcess[i].ProcessGroup.data.name;
			$scope.treeNode.children = [];
			if (allGroupsWithProcess[i].ProcessStep.length > 0) {
				for (var j = 0; j < allGroupsWithProcess[i].ProcessStep.length; j++) {
					$scope.ProcessStep = {};
					$scope.ProcessStep.name = allGroupsWithProcess[i].ProcessStep[j].data.name;
					$scope.ProcessStep.itemID = allGroupsWithProcess[i].ProcessStep[j].data.ID;
					if (allGroupsWithProcess[i].ProcessStep[j].data.ID == $scope.currentStep) {
						$scope.parentID = i;
						$scope.childID = j;
					}
					$scope.treeNode.children = $scope.treeNode.children.concat($scope.ProcessStep);
				}
			}
			$scope.parent = $scope.parent.concat($scope.treeNode);
		}
		$scope.dataForTheTree = $scope.parent;

		$scope.selectedNode = $scope.dataForTheTree[$scope.parentID].children[$scope.childID];
		$scope.nodeHead = [$scope.parent[$scope.parentID]];
		/*
		 $scope.selectedNode = $scope.dataForTheTree[0].children[0];
		 $scope.nodeHead = [$scope.parent[0].children[0]];
		 */
	};

	$scope.treeOptions = {
		nodeChildren : "children",
		dirSelectable : true
	};


	$scope.showSelected = function(node) {
		console.log(node.itemID);

		$http.get(base_url + '/assets/csv/pStepViewMapper.json').success(function(data) {
			$scope.processMaps = data;
			angular.forEach($scope.processMaps, function(processMap) {
				if (processMap.id == node.itemID) {
					//console.log(processMap);
					$location.path(processMap.view);
				}
			});
		});
	}; 
    
    $scope.LoadAssociatedProcessGroupsWithProcess = function(associatedSteps) {
		var componentID;
		$scope.selectedComponent = ComponentShareService.getSelectedComponent();
		if ($scope.selectedComponent != false) {
			componentID = $scope.selectedComponent.data.component_id;
			console.log("selected component from components view is .....");
			console.log(componentID);
			
			ProcessStepsService.GetAllProcessGroupWithProcessSteps(componentID).then(function(result) {
				$scope.allGroupsWithProcess = result.data.responseData;
				
				if ($scope.allGroupsWithProcess.length > 0) {
					$scope.LoadAssociatedStepsTree($scope.allGroupsWithProcess,associatedSteps);
				}

			});
		}
	};

	$scope.LoadAssociatedStepsTree = function(allGroupsWithProcess,associatedSteps) {

		$scope.parent = [];

		for (var i = 0; i < allGroupsWithProcess.length; i++) {

			$scope.treeNode = {};
			$scope.treeNode.name = allGroupsWithProcess[i].ProcessGroup.data.name;
			$scope.treeNode.children = [];
			if (allGroupsWithProcess[i].ProcessStep.length > 0) {
				for (var j = 0; j < allGroupsWithProcess[i].ProcessStep.length; j++) {
				
					
					
					for (var associatedStep = 0; associatedStep < associatedSteps.length; associatedStep++) {
						var stepIDD = associatedSteps[associatedStep].step.data.id;
						console.log("####################################");
						console.log(stepIDD);
						console.log("/////////////////////////////////////");
						if (stepIDD == allGroupsWithProcess[i].ProcessStep[j].data.ID) {
							$scope.ProcessStep = {};
							$scope.ProcessStep.name = allGroupsWithProcess[i].ProcessStep[j].data.name;
							$scope.ProcessStep.itemID = allGroupsWithProcess[i].ProcessStep[j].data.ID;
							if (allGroupsWithProcess[i].ProcessStep[j].data.ID == $scope.currentStep) {
								$scope.parentID = i;
								$scope.childID = j;
							}
							$scope.treeNode.children = $scope.treeNode.children.concat($scope.ProcessStep);
						}
					}

                    /*
					$scope.ProcessStep = {};
					$scope.ProcessStep.name = allGroupsWithProcess[i].ProcessStep[j].data.name;
					$scope.ProcessStep.itemID = allGroupsWithProcess[i].ProcessStep[j].data.ID;
					if (allGroupsWithProcess[i].ProcessStep[j].data.ID == $scope.currentStep) {
						$scope.parentID = i;
						$scope.childID = j;
					}
					$scope.treeNode.children = $scope.treeNode.children.concat($scope.ProcessStep);
					*/
				}
			}
			$scope.parent = $scope.parent.concat($scope.treeNode);
		}
		$scope.dataForTheTree = $scope.parent;

		$scope.selectedNode = $scope.dataForTheTree[$scope.parentID].children[$scope.childID];
		$scope.nodeHead = [$scope.parent[$scope.parentID]];
		/*
		 $scope.selectedNode = $scope.dataForTheTree[0].children[0];
		 $scope.nodeHead = [$scope.parent[0].children[0]];
		 */
	};
    
    
	$scope.LoadProcessGroupsWithProcess();
}); 