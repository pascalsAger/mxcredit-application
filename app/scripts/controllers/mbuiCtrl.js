mctApp.controller('MbuiCtrl', function($scope, $rootScope, $location, MbuiService, $modal, $http) {
	if ( typeof $rootScope.loggedin == 'undefined' || $rootScope.loggedin == false) {
		$location.path('/login');
		return;
	}

	$rootScope.leftPanel = false;
	$scope.selectedRow = null; 
	$scope.groups;
	$scope.processSteps;
	$scope.schema;
	$scope.stepID;

	$scope.GetProcessSteps = function() {
		var nextStep;
		$http.get('http://localhost:4000/assets/csv/pStepViewMapper.json').success(function(data) {
			$scope.processSteps = data;
		});
	};
	
	$scope.GetProcessSteps();
	
	$scope.LoadProcessStepUI = function(pStep,index) {
		$scope.stepID = pStep.id;
		$scope.stepName = pStep.name;
        $scope.selectedRow = index; 
		MbuiService.GetSchema($scope.stepID).then(function(result) {
			$scope.data = result.data.responseData;
			if ($scope.data.length > 0) {
				$scope.schema = $scope.data[0].pst.data.json_schema;
			}
			console.log("****schema****");
			console.log($scope.schema);
		});
	}; 

	
	$scope.UpdateSchema = function() {
		console.log($scope.schema);
		console.log($scope.stepID);
		MbuiService.UpdateSchema($scope.stepID, $scope.schema).then(function(result) {
					console.log(result);
					//$scope.LoadLinks();					
				});
	};
}); 