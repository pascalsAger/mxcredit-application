mctApp.controller('GroupCtrl', function ($scope, $rootScope, $location, GroupService,$modal,ProcessStepsService,PStepGroupServer) {
    if (typeof  $rootScope.loggedin == 'undefined' || $rootScope.loggedin == false) {
        $location.path('/login');
        return;
    }
    
   $rootScope.leftPanel = false; 
   $scope.groups;
   $scope.groupId;
    
   $scope.LoadGroupDetail= function(groupID) {
	        GroupService.GetGroupById(groupID).then(function(result) {
			if(result.data.responseData.length > 0)
			{
				$scope.group = result.data.responseData[0].group;
				$scope.name = $scope.group.data.name;
				$scope.type = $scope.group.data.type;	
				$scope.description = $scope.group.data.description;
			}
			
			console.log($scope.group);
		});
		$scope.groupId = groupID;
		$scope.LoadGroupUsers(groupID);	
		$scope.LoadNotLinkedProcessSteps(groupID);
		$scope.LoadLinkedProcessSteps(groupID);
	};
	
	 $scope.LoadGroups= function() 
	 {
	        GroupService.GetAllGroups().then(function(result) {
			
			$scope.groups = result.data.responseData;
		});	
	};
	
	$scope.LoadGroupUsers= function(groupID) {
		
		GroupService.GetGroupUsers(groupID).then(function(result) {
		$scope.groupUsers = result.data.responseData;	
		}); 
	};
	
    $scope.newGroup = {"name": "", "type": "", "description": ""};
    
    
    $scope.createGroup = function (newGroup) {
    // console.log(newGroup.name);
    };

    var ModalInstanceCtrl = function ($scope, $modalInstance,newGroup,GroupService) {
        $scope.newGroup = newGroup;
        $scope.submit = function () {
        	        	
        	GroupService.AddNewGroup($scope.newGroup).then(function(result) {
			console.log(result);
		    $modalInstance.close(); 
		   });
          
         // $modalInstance.close($scope.newGroup);
        };
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    };

    $scope.open = function () {
        var modalInstance = $modal.open({
            templateUrl: 'views/modals/createGroup.html',
            controller: ModalInstanceCtrl,
            resolve: {
                newGroup: function () {
                    return $scope.newGroup;
                }
            }
        });
        modalInstance.result.then(function () {
            $scope.LoadGroups();
        }, function () {
        });
    };
    
    $scope.removeGroup = function (group) {
        GroupService.delete({"group": group.id}, function (data) {
            $scope.groups = GroupService.get();
        });
    };

    $scope.cancel = function () {
        $scope.newGroup.id = "";
        $scope.newGroup.name = "";
    };

    $scope.query = "";
    
    $scope.LoadNotLinkedProcessSteps= function(groupID) 
	 {
	 	
	        PStepGroupServer.GetAllNotLinkedSteps(groupID).then(function(result) {
			
			$scope.notLinkedProcessSteps = result.data.responseData;
		});	
	};
	 $scope.LoadLinkedProcessSteps= function(groupID) 
	 {
	 	
	        PStepGroupServer.GetLinkedSteps(groupID).then(function(result) {			
			$scope.linkedProcessSteps = result.data.responseData;
		});	
	};
	

	
	$scope.addLink = function(stepID) {
		console.log(stepID);
		console.log($scope.groupId);

		PStepGroupServer.AddStepLinkToGroup($scope.groupId, stepID).then(function(result) {

			$scope.linked = result.data.responseData;
			PStepGroupServer.GetAllNotLinkedSteps($scope.groupId).then(function(result) {

				$scope.notLinkedProcessSteps = result.data.responseData;
				PStepGroupServer.GetLinkedSteps($scope.groupId).then(function(result) {
					$scope.linkedProcessSteps = result.data.responseData;
				});
			});
		});
	}; 
 
	
	$scope.removeAssociation = function(stepID) {
		console.log(stepID);
		console.log($scope.groupId);

		PStepGroupServer.DeleteGroupStepLinkAssociation($scope.groupId, stepID).then(function(result) {
			$scope.deletedRelation = result.data.responseData;
			PStepGroupServer.GetAllNotLinkedSteps($scope.groupId).then(function(result) {

				$scope.notLinkedProcessSteps = result.data.responseData;
				PStepGroupServer.GetLinkedSteps($scope.groupId).then(function(result) {
					$scope.linkedProcessSteps = result.data.responseData;
				});
			});
		});

	};


   // $scope.LoadProcessSteps();
});