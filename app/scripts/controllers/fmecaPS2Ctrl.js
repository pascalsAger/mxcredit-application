mctApp.controller('FmecaPS2Ctrl', function($scope, $rootScope, $location, ProcessStepsService, ComponentShareService, ProcessEngineService, $http) {
	if ( typeof $rootScope.loggedin == 'undefined' || $rootScope.loggedin == false) {
		$location.path('/login');
		return;
	}

	$rootScope.leftPanel = false;
	$scope.leftGroupProcessTree = true;
    $scope.selectedComponent = ComponentShareService.getSelectedComponent();
	$scope.currentStep = 2;
	$scope.destinationProcessID;
	
	ProcessEngineService.GetCurrentProcessStepDetails($scope.selectedComponent.data.component_id,$scope.currentStep).then(function(result) {
			$scope.Process = result;
			$scope.destinationProcessID = result.data.responseData[0].destinationProcess.data.ID;			

	});

	$scope.Submit = function() {

		$scope.LoadActiveProcessStep($scope.destinationProcessID);

	}; 

	
	$scope.GetNextProcessStep = function( processStepID) {
       var nextStep;
		$http.get('http://localhost:4000/assets/csv/pStepViewMapper.json').success(function(data) {
			$scope.processMaps = data;
			angular.forEach($scope.processMaps, function(processMap) {
				if(processMap.id == processStepID)
				{
					nextStep = processMap;
				}				
			});
		});
		return nextStep;
	};
	$scope.LoadActiveProcessStep = function(activeProcessID) {
		$http.get('http://localhost:4000/assets/csv/pStepViewMapper.json').success(function(data) {
			$scope.processMaps = data;
			angular.forEach($scope.processMaps, function(processMap) {
				if(processMap.id == activeProcessID)
				{
					//console.log(processMap);
					$location.path(processMap.view);	
				}				
			});
		});
	};
	/*if (angular.isDefined($scope.selectedComponent.data.component_id)) {
		ProcessEngineService.GetAllSubActiveProcess($scope.selectedComponent.data.component_id).then(function(result) {
			$scope.activeProcess = result;
			if ($scope.activeProcess.data.responseData.length > 0) {
				$scope.LoadActiveProcessStep($scope.activeProcess.data.responseData[0].sourceProcess.data.ID);
			}
		});
	}*/
   /*
	$scope.LoadActiveProcessStep = function(activeProcessID) {
		$http.get('http://localhost:4000/assets/csv/pStepViewMapper.json').success(function(data) {
			$scope.processMaps = data;
			angular.forEach($scope.processMaps, function(processMap) {
				if(processMap.id == activeProcessID)
				{
					//console.log(processMap);
					$location.path('subComponent');	
				}				
			});
		});
	};
	*/
});
