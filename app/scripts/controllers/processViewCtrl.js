mctApp.controller('ProcessViewCtrl', function($scope, $rootScope, $location, ProcessStepsService, ComponentShareService, Upload,$http) {
	if ( typeof $rootScope.loggedin == 'undefined' || $rootScope.loggedin == false) {
		$location.path('/login');
		return;
	}

	$rootScope.leftPanel = false;
	$rootScope.setProcessViewActive();
	$scope.leftGroupProcessTree = true;
    $scope.currentStep = 1;
	$scope.AddValue = function() {
		$scope.value = $scope.data_source;
	};
	
    var base_url = window.location.origin;
  	$scope.processSVG = base_url+"/assets/csv/Process_View_Sample_171215.svg";
	/*
   $http.get(base_url + 'assets/js/angularxml/courseDef.xml').then(function(response) {
    var chapters = [];

    var courseDef = x2js.xml_str2json(response.data);
    $scope.chaptersObj = courseDef.course.navigation.chapter;

    
    var numOfChapters = $scope.chaptersObj.length;
    for (var i = 0; i < numOfChapters; i++) {
      chapters.push({
        name: $scope.chaptersObj[i].name,
        number: $scope.chaptersObj[i]._number
      });
    }

    $scope.chapterNames = chapters;
  });
  */
  
});
