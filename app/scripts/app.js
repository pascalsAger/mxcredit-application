var mctApp = angular.module('mctApp', ['ngResource', 'ui.bootstrap', "ngRoute" , 'angularMoment','treeControl','ngFileUpload','schemaForm','ngSchemaFormFile','ngAnimate','ngCookies'])
    .config(['$routeProvider', function ($routeProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'api/views/login.html',
                controller: 'LoginCtrl'
            })
            .when('/dashboard', {
                templateUrl: 'api/views/dashboard.html',
                controller: 'DashboardCtrl'
            }).when('/users', {
                templateUrl: 'api/views/users.html',
                controller: 'UsersCtrl'
            }).when('/groups', {
                templateUrl: 'api/views/manage_dashboard.html',
                controller: 'GroupCtrl'
            }).when('/tasks', {
                templateUrl: 'api/views/tasks.html',
                controller: 'TasksCtrl'
            }).when('/processes', {
                templateUrl: 'api/views/processes.html',
                controller: 'ProcessesCtrl'
            }).when('/instances', {
                templateUrl: 'api/views/instances.html',
                controller: 'InstancesCtrl'
            }).when('/addComponent', {
                templateUrl: 'api/views/add_component.html',
                controller: 'ComponentCtrl'
            }).when('/addSubComponent', {
                templateUrl: 'api/views/add_sub_component.html',
                controller: 'SubComponentCtrl'
            }).when('/subComponent', {
                templateUrl: 'api/views/sub_component.html',
                controller: 'SubComponentCtrl'
            }).when('/addItem', {
                templateUrl: 'api/views/add_item.html',
                controller: 'ItemCtrl'
            }).when('/items', {
                templateUrl: 'api/views/items.html',
                controller: 'ItemCtrl'
            }).when('/processSteps', {
                templateUrl: 'api/views/process_steps_dashboard.html',
                controller: 'ProcessStepsCtrl'
            }).when('/processView', {
                templateUrl: 'api/views/process_view.html',
                controller: 'ProcessViewCtrl'
            }).when('/processViewTabular', {
                templateUrl: 'api/views/process_view_tabular.html',
                controller: 'ProcessViewTabularCtrl'
            }).when('/forms/:id/view', {
                templateUrl: 'api/views/view.html',
                controller: 'ViewCtrl'
            }).when('/mbui', {
                templateUrl: 'api/views/model_based_ui.html',
                controller: 'MbuiCtrl'
            }).when('/componentDetails', {
                templateUrl: 'api/views/ps1_component_details.html',
                controller: 'PS1ComponentDetailsCtrl'
            }).when('/fmea', {
                templateUrl: 'api/views/ps2_fmea.html',
                controller: 'PS2FmeaCtrl'
            }).when('/fmeca', {                   // steps here.......
                templateUrl: 'api/views/ps3_fmeca.html',
                controller: 'PS3FmecaCtrl'
            }) .when('/rcmmaintanance', {
                templateUrl: 'api/views/ps4_rcm_maintanance.html',
                controller: 'PS4RCMmaintananceCapCtrl'
            }).when('/rcmanalysis', {
                templateUrl: 'api/views/ps5_rcm_analysis.html',
                controller: 'PS5RCMAnalysisCtrl'
            })
                                       
            
            .otherwise({
                redirectTo: '/'
            }); 
    }]);