mctApp.service('PStepGroupServer', function ($resource,$http) {
     
     this.GetAllNotLinkedSteps = function(groupID) {
            var steps = $http.get("/api/stepGroup/GetNotLinkedProcessSteps/"+groupID);                        
            return steps;
       };
       
      this.GetLinkedSteps = function(groupID) {
            var steps = $http.get("/api/stepGroup/GetLinkedProcessSteps/"+groupID);                        
            return steps;
       }; 
       
       this.AddStepLinkToGroup = function(groupID,stepID) {         
		   
		    var query = [  "Match (g:Group) where ID(g) = "+ groupID +" Match (step:ProcessStepTemplate) where ID(step) = "+ stepID +" ",
							"CREATE (g)-[:HAS]-> (step) RETURN g, step"
							].join('\n');
		      
           var call = $http({
                method: 'POST',
                data: { query: query },
                url: '/api/stepGroup/GroupLinkToStep',
                headers: { 'Content-Type': 'application/json' }
            });
            return call;
       };
       
        this.DeleteGroupStepLinkAssociation = function(groupID,stepID) {         
		   
		    var query = [  "Match (g:Group) where ID(g) = "+ groupID +" Match (step:ProcessStepTemplate) where ID(step) = "+ stepID +" ",
							"MATCH (g)-[r:HAS]-> (step) DELETE r"
							].join('\n');
		      
           var call = $http({
                method: 'POST',
                data: { query: query },
                url: '/api/stepGroup/RemoveStepGroupAssociation',
                headers: { 'Content-Type': 'application/json' }
            });
            return call;
       };
});