mctApp.service('ItemService', function ($resource,$http) {
   

    this.GetItems = function(componentID,subComponentName) {         
		   
		    var query = [   "MATCH (component:Component { component_id:'"+ componentID +"'  })",
		                    "-[:HAS_SUB_COMPONENT]->(subComponent:SubComponent{sub_component_name:'"+ subComponentName +"'})",
		                    "-[:HAS_ITEM]-> (Item) return Item"
							].join('\n');
		      
           var call = $http({
                method: 'POST',
                data: { query: query },
                url: '/api/item/GetItems',
                headers: { 'Content-Type': 'application/json' }
            });
            return call;
       };


    //TODO: find the right script
    this.GetFailureModes = function(componentID,subComponentName) {

        var query = [   "MATCH (component:Component { component_id:'"+ componentID +"'  })",
            "-[:HAS_SUB_COMPONENT]->(subComponent:SubComponent{sub_component_name:'"+ subComponentName +"'})",
            "-[:HAS_ITEM]-> (item:Item{sub_component_name:'"+ subComponentName +"'}) return item"
        ].join('\n');

        var call = $http({
            method: 'POST',
            data: { query: query },
            url: '/api/item/GetFailureModes',
            headers: { 'Content-Type': 'application/json' }
        });
        return call;
    };
  
      
    this.AddNewItem = function(componentID,subComponentName,item) {         
		   
		    var query = [   " MATCH (component:Component { component_id:'"+ componentID +"' })",
							"-[:HAS_SUB_COMPONENT]->",
							"(subComponent:SubComponent{sub_component_name:'"+ subComponentName +"'})",
							"  Create(item:Item{",
							"item_name:'"+ item.Item.data.item_name +"',",
							"sbc:'"+ item.Item.data.sbc +"',",
							"part_no:'"+ item.Item.data.part_no +"',",							
							"supplier_name:'"+ item.Item.data.supplier_name +"',",
							"supplier_details:'"+ item.Item.data.supplier_details +"'})",
							"Create (subComponent)-[:HAS_ITEM]->(item) return item"
							].join('\n');
		      
           var call = $http({
                method: 'POST',
                data: { query: query },
                url: '/api/item',
                headers: { 'Content-Type': 'application/json' }
            });
            return call;
       };
       
       this.DeleteItemById = function(itemId) {
        return retuls = $http.delete("/api/item/"+itemId); 
        };
       
});