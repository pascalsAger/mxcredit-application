mctApp.service('LinkService', function ($resource,$http) {
   

	this.GetLinks = function(componentID,processID) {
		
		var links = $http.get("/api/link/GetLinks/"+componentID+"/"+processID);
		return links;
	};


    this.AddLink = function(componentID,processID,url,name,description) {         
		   
		   var query = [   " MATCH (component:Component { component_id: '"+ componentID +"' })-[:HAS]-(mxCredit:MxCreditProcess)-[:HAS]-(pg:ProcessGroup)-[:HAS]-",
							"(process:ProcessStep{ID:"+ processID +"})",
							"CREATE (link:Link {url:'"+ url +"',name:'"+ name +"',description:'"+ description +"'})",
							"CREATE (process)- [:HAS] -> (link)	",
							"return link"].join('\n');
		      
           var call = $http({
                method: 'POST',
                data: { query: query },
                url: '/api/link',
                headers: { 'Content-Type': 'application/json' }
            });
            return call;
       };
       
      
	this.DeleteLinkById = function(linkID) {
		return retuls = $http.delete("/api/link/" + linkID);
	}; 

       
});