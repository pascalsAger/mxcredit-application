mctApp.service('GroupService', function ($resource,$http) {
     
     this.GetAllGroups = function() {
            var groups = $http.get("/api/group");                             
            return groups;
       };
       
       this.GetGroupById = function(groupID) {
    	   
            var group = $http.get("/api/group/"+groupID);                             
            return group;
       };   
       
         this.GetGroupUsers = function(groupID) {
    	   
            var users = $http.get("/api/group/allusers/"+groupID);                             
            return users;
       };
       
       this.AddNewGroup = function(newGroup) {         
		   
		    var query = [" MATCH (stakeHolder:StakeHolder)",
							"CREATE (group:Group {name:'"+ newGroup.name +"',",
							"type:'"+ newGroup.type +"',",
							"description:'"+ newGroup.description +"'})",
							"CREATE (stakeHolder)- [:HAS_GROUP] -> (group)",
							" Return group"].join('\n');
		      
           var call = $http({
                method: 'POST',
                data: { query: query },
                url: '/api/group',
                headers: { 'Content-Type': 'application/json' }
            });
            return call;
       };
});