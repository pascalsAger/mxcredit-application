'use strict';

//angularApp.service('FormService', function FormService($http) {
mctApp.service('FormService', function FormService($http) {

	var formsJsonPath = './static-data/sample_forms.json';

	


	return {
		form : function(id) {
			// $http returns a promise, which has a then function, which also returns a promise
			return $http.get(formsJsonPath).then(function(response) {
				var requestedForm = {};
				angular.forEach(response.data, function(form) {
					//if (form.form_id == id)
					requestedForm = form;
				});
				return requestedForm;
			});
		}
	};

 
		/*
		 this.GetProcessSchema = function() {
		 return $http.get("/api/processSteps/GetProcessSchema").then(function(response) {
		 var requestedForm = angular.fromJson(response.data);
		 return requestedForm;
		 });
		 };
		 */

   
	this.AddDataToProcessStep = function(formData) {

		var query = ["CREATE (dataTable:DataTable {schema:'" + formData + "'})return dataTable"].join('\n');

		var call = $http({
			method : 'POST',
			data : {
				query : query
			},
			url : '/api/processSteps/AddProcessData',
			headers : {
				'Content-Type' : 'application/json'
			}
		});
		return call;
	};	

});
