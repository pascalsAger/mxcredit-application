mctApp.service('MbuiService', function($resource, $http) {

	this.GetSchema = function(processStepID) {
		var schema = $http.get("/api/processSchema/GetProcessStepSchema/" + processStepID);
		return schema;
	};

	this.UpdateSchema = function(processID, schema) {

		var query = ["MATCH (:ProcessSchemaTemplateMaster)-[:HAS]-(pst:ProcessStepTemplate{ id: "+ processID +" }) SET pst.json_schema = '"+ schema +"' RETURN pst"].join('\n');
          
		var call = $http({
			method : 'POST',
			data : {
				query : query
			},
			url : '/api/processSchema',
			headers : {
				'Content-Type' : 'application/json'
			}
		});
		return call;
	};

}); 