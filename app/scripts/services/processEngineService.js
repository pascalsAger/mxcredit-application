mctApp.service('ProcessEngineService', function($resource, $http) {

	this.GetAllSubActiveProcess = function(componentId) {
		console.log(componentId);
		var activeProcess = $http.get("/api/processEngine/getActiveProcess/" + componentId);
		return activeProcess;
	};

	this.GetCurrentProcessStepDetails = function(componentID, processStepID) {

		// console.log(componentId);
		var activeProcess = $http.get("/api/processEngine/getProcessDetails/" + componentID + "/" + processStepID);
		//var activeProcess = $http.get("/api/processEngine/getProcessDetails/CC4/1");
		return activeProcess;
	};
	
	this.GetLastProcessStepDetails = function(componentID, processStepID) {

		// console.log(componentId);
		var activeProcess = $http.get("/api/processEngine/getLastProcessDetails/" + componentID + "/" + processStepID);
		//var activeProcess = $http.get("/apiprocessEngine/getProcessDetails/CC4/1");
		return activeProcess;
	};
	
	this.GetCurrentProcessStepSchema = function(componentID, processStepID) {

		// console.log(componentId);
		var activeProcess = $http.get("/api/processEngine/getProcessStepSchema/" + componentID + "/" + processStepID);
		//var activeProcess = $http.get("/api/processEngine/getProcessDetails/CC4/1");
		return activeProcess;
	};
	
	this.AddDataToProcessStep = function(componentId,processStepId,formData) {

		var  query = ["MATCH (component:Component { component_id: '"+ componentId +"' })-[:HAS]-(mxCredit:MxCreditProcess)-[:HAS]-",
         "(pg:ProcessGroup)-[:HAS]-(sourceProcess:ProcessStep{ID:"+ processStepId +"})-[:HAS_DATA_FIELD]-> (dataField:DataField)  SET dataField.json_schema= '" + formData + "' return dataField"].join('\n');
        //console.log(query);

		var call = $http({
			method : 'POST',
			data : {
				query : query
			},
			url : '/api/processEngine/UpdateSchemaWithData',
			headers : {
				'Content-Type' : 'application/json'
			}
		});
		return call;
	};	
	
	this.FinalDataSubmit = function(processStepId) {

		var  query = ["Match (step:ProcessStep) where ID(step) = "+ processStepId +" ",
         "set step.isDataSubmitted = 'TRUE' return step "].join('\n');

		var call = $http({
			method : 'POST',
			data : {
				query : query
			},
			url : '/api/processEngine/UpdateProcessStatus',
			headers : {
				'Content-Type' : 'application/json'
			}
		});
		return call;
	};
	
	this.StateSave = function(processStepId) {

		var  query = ["Match (step:ProcessStep) where ID(step) = "+ processStepId +" ",
         "set step.isActive = 'TRUE' return step "].join('\n');

		var call = $http({
			method : 'POST',
			data : {
				query : query
			},
			url : '/api/processEngine/UpdateProcessStatus',
			headers : {
				'Content-Type' : 'application/json'
			}
		});
		return call;
	};
	
}); 