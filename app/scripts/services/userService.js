mctApp.service('UserService', function ($resource,$http) {
   
      
    this.GetLogin = function(email,password) {
           var query = "MATCH (user:User { email: '"+ email +"',password: '"+ password +"' })"; 
           query += " RETURN user ";
           var call = $http({
                method: 'POST',
                data: { query: query },
                url: '/api/user/GetLoginData',
                headers: { 'Content-Type': 'application/json' }
            });
            return call;
       };
       
       
      this.GetAllUser = function() {
    	
            var users = $http.get('/api/user');
            //console.log(users);
            return users;
       };
       
       this.GetUserById = function(userID) {
    	   
            var group = $http.get("/api/user/"+userID);                             
            return group;
       };
       
       this.GetUserGroups = function(userID) {
    	   
            var groups = $http.get("/api/user/allgroups/"+userID);                             
            return groups;
       };  
       
      this.AddNewUser = function(newUser) {         
		    
		    var query = ["CREATE (user:User",
						  "{display_name:'"+ newUser.display_name +"',",
						  "first_name:'"+ newUser.first_name +"',",
						  "last_name:'"+ newUser.last_name +"',",
						  "email:'"+ newUser.email +"',",
						  "password:'"+ newUser.password +"'})",
							" Return user"].join('\n');
		      
           var call = $http({
                method: 'POST',
                data: { query: query },
                url: '/api/user',
                headers: { 'Content-Type': 'application/json' }
            });
            return call;
       };
       
       this.AddUserRelGroup = function(userID,groupID) {         
		    
		    var query = ["Match(group:Group) Where ID(group) = "+ groupID +"",
						  "Match(user:User) Where ID(user) = "+ userID +"",
						  "Create (group)- [:HAS_USER] -> (user)",
						  "return group,user"].join('\n');
		      
           var call = $http({
                method: 'POST',
                data: { query: query },
                url: '/api/user/userRelgroup',
                headers: { 'Content-Type': 'application/json' }
            });
            return call;
       };
});