mctApp.service('FileService', function ($resource,$http) {
   

	this.GetFiles = function(componentID,processID) {
		
		var files = $http.get("/api/file/GetFiles/"+componentID+"/"+processID);
		return files;
	};


    this.AddFile = function(componentID,processID,fileName,fileNameCustom) {         
		   
		   var query = [   " MATCH (component:Component { component_id: '"+ componentID +"' })-[:HAS]-(mxCredit:MxCreditProcess)-[:HAS]-(pg:ProcessGroup)-[:HAS]-",
							"(process:ProcessStep{ID:"+ processID +"})",
							"CREATE (dataFile:DataFile {name:'"+ fileName +"',file_custom_name:'"+ fileNameCustom +"'})",
							"CREATE (process)- [:HAS] -> (dataFile)	",
							 "return dataFile"].join('\n');
		      
           var call = $http({
                method: 'POST',
                data: { query: query },
                url: '/api/file',
                headers: { 'Content-Type': 'application/json' }
            });
            return call;
       };
       
       this.DeleteFileById = function(fileID) {
        return retuls = $http.delete("/api/file/"+fileID); 
        };
       
});