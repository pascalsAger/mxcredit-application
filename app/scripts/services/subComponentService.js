mctApp.service('SubComponentService', function ($resource,$http) {
   
   
    this.GetAllSubComponent = function(componentId) {
    	   console.log(componentId);
            var subComponents = $http.get("/api/subComponent/"+componentId); 
                             /*
                                  $http({
								    url: '/api/subComponent', 
								    method: "GET",
								    params: {componentId: componentId}
								 });
						    */
            return subComponents;
       };
  
      
    this.AddNewSubComponent = function(subComponent,componentID) {         
		   
		    var query = [   " MATCH (component:Component { component_id:'"+ componentID +"' })",
							"Create(subComponent:SubComponent { ",
							"sub_component_name:'"+ subComponent.sub_component_name +"',",
							"sbc:'"+ subComponent.sbc +"',",
							"part_no:'"+ subComponent.part_no +"',",							
							"supplier_name:'"+ subComponent.supplier_name +"',",
							"supplier_details:'"+ subComponent.supplier_details +"'})",
							"Create (component)-[:HAS_SUB_COMPONENT]->(subComponent) return subComponent"
							].join('\n');
		      
           var call = $http({
                method: 'POST',
                data: { query: query },
                url: '/api/subComponent',
                headers: { 'Content-Type': 'application/json' }
            });
            return call;
       };
       
});