mctApp.service('ComponentShareService', function ($resource,$http) {
  
   var compomnetShare = {};
	  compomnetShare.component = false;
	  
	  compomnetShare.addSelectedComponent = function(data){
	      this.component = data;
	  };
	  
	  compomnetShare.getSelectedComponent = function(){
	    return this.component;
	  };
	  
	  compomnetShare.removeSelectedComponent = function(){
	     compomnetShare.component = false;
	  };
	  
	  /*
	 var subCompomnetShare = {};
	  subCompomnetShare.subComponent = false;
	  
	  subCompomnetShare.addSelectedSubComponent = function(data){
	      this.subComponent = data;
	  };
	 
	  subCompomnetShare.getSelectedSubComponent = function(){
	    return this.subComponent;
	  };  
	  */
  return compomnetShare;
       
});

mctApp.service('SubComponentShareService', function ($resource,$http) {
  
      var subCompomnetShare = {};
	  subCompomnetShare.subComponent = false;
	  
	  subCompomnetShare.addSelectedSubComponent = function(data){
	      this.subComponent = data;
	  };
	  
	  subCompomnetShare.getSelectedSubComponent = function(){
	    return this.subComponent;
	  };
	  
	  
	  
  return subCompomnetShare;
       
});

