
// var neo4j = require('neo4j-js');
mctApp.service('componentService', function ($resource,$http) {
   //var data = $resource('http://localhost:8080/activiti-rest/service/identity/users/:user', {user: "@user"});
   
   
    this.GetAllComponent = function() {
    	
            var nodes = $http.get('/api/component');
            return nodes;
       };
 
     this.GetAllComponentSubComponent = function() {
    	
            var nodes = $http.get('/api/component/componentSubComponent');
            return nodes;
       }; 
      
    this.AddNewComponent = function(component) {         
		   
		    var query = [  "MERGE (id:UniqueId{name:'UniqueComponent',str:'CC'})",
							"ON CREATE SET id.count = 1",
							"ON MATCH SET id.count = id.count + 1",
							"WITH id.str + id.count AS uid",
							"Match(sub_system:SubSystem)",
							"CREATE (component:Component {",
							"component_id:uid,",
							"component_name:'"+ component.component_name +"',",
							"ata_chapter:'"+ component.ata_chapter +"',",
							"sbc:'"+ component.sbc +"',",
							"partNo:'"+ component.partNo +"',",
							"supplier_name:'"+ component.supplier_name +"',", <!--Advith-->
							"supplier_details:'"+ component.supplier_details +"'})",
							"CREATE(sub_system)-[:HAS_COMPONENT]-> (component) return component"
							].join('\n');
		      
           var call = $http({
                method: 'POST',
                data: { query: query },
                url: '/api/component',
                headers: { 'Content-Type': 'application/json' }
            });
            return call;
       };
       
});