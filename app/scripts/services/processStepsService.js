
mctApp.service('ProcessStepsService', function ($resource,$http,$location) {
   
   var loc = $location.path();
    this.IsMxCreditProcessAvailable = function(componentId) {
    	   //console.log("from process steps service");
    	  console.log(loc);
            var processSteps = $http.get("/api/processSteps/"+componentId);
            // console.log(processSteps); 
            return processSteps;
       };
  
      
    this.CreateMxCreditProcess = function(componentID) {         
		   console.log("in service create process "+ componentID);
		  // console.log(loc);
		    
		    var query = [   " MATCH (component:Component { component_id:'"+ componentID +"' })",
							"Create(mxCredit:MxCreditProcess) ",
							"Create (component)- [:HAS] -> (mxCredit)",
							"Create(swlane1:Swimlane {name:'Aircraft Development',ID:1} )",
							"Create(swlane2:Swimlane {name:'Mx Concept Development',ID:2} )",
							"Create(swlane3:Swimlane {name:'ISHM System Development',ID:3} )",
							"Create(swlane4:Swimlane {name:'Mx Credit Certification',ID:4} )",
							"Create(psg1:ProcessGroup {name:'System Component Selection',ID:1} )",
							"Create(psg2:ProcessGroup {name:'System Design Assessment',ID:2})",							
							"Create(psg3:ProcessGroup {name:'RCM with Mx Task Development',ID:3})",							
							"Create (mxCredit)- [:HAS] -> (swlane1)",
							"Create (mxCredit)- [:HAS] -> (swlane2)",
							"Create (mxCredit)- [:HAS] -> (swlane3)",
							"Create (mxCredit)- [:HAS] -> (swlane4)",
							"Create (mxCredit)- [:HAS] -> (psg1)",
							"Create (mxCredit)- [:HAS] -> (psg2)",
							"Create (mxCredit)- [:HAS] -> (psg3)"
							].join('\n');
		      
           var call = $http({
                method: 'POST',
                data: { query: query },
                url: '/api/processSteps/processStep',
                headers: { 'Content-Type': 'application/json' }
            });
            return call;
       };
       
     this.AddAllProcessSteps = function(componentID) {         
		   console.log("in service create process "+ componentID);
		   
		    var query = [   "LOAD CSV WITH HEADERS FROM \"http://127.0.0.1:4000/assets/csv/process_steps.csv\" AS csvLine",							
							"MATCH (component:Component { component_id: '"+ componentID +"' })-[:HAS] ->(mxCredit:MxCreditProcess)-[:HAS]->(pswl:Swimlane{ID:toInt(csvLine.swimlane)})",
							"MATCH (component:Component { component_id: '"+ componentID +"' })-[:HAS] ->(mxCredit:MxCreditProcess)-[:HAS]->(pg:ProcessGroup{ID:toInt(csvLine.processGroup)})",
							"MATCH (:ProcessSchemaTemplateMaster)-[:HAS]-(pst:ProcessStepTemplate{ id:toInt(csvLine.id)}) ",
							"CREATE (ps:ProcessStep { ID: toInt(csvLine.id), name: csvLine.name, type:csvLine.type,isDataSubmitted:csvLine.isDataSubmitted,isActive:csvLine.isActive })",
							"CREATE(dataField:DataField {json_schema:'JSON'})",		
							"CREATE(data:Data)",
							"CREATE (ps) -[:HAS_DATA_FIELD] ->(dataField)",
							"CREATE (ps) -[:HAS_DATA] ->(data)",
							"CREATE (pswl)- [:HAS] -> (ps)",
							"CREATE (pg)- [:HAS] -> (ps)",
							"SET dataField = pst"
							].join('\n');
		      
           var call = $http({
                method: 'POST',
                data: { query: query },
                url: '/api/processSteps/AddAllProcessSteps',
                headers: { 'Content-Type': 'application/json' }
            });
            return call;            
       };  
       
      this.AddRulesToSteps = function(componentID) {         
		   console.log("in service create process "+ componentID);
		   
		    var query = [   "LOAD CSV WITH HEADERS FROM \"http://127.0.0.1:4000/assets/csv/step_rules.csv\" AS csvLine",
							"MATCH (component:Component { component_id:'"+ componentID +"' })",
							"MATCH component -[:HAS]-(mxCredit:MxCreditProcess) -[:HAS]-(processGroup:ProcessGroup) -[:HAS]-(sourceProcessStep:ProcessStep{ID:toInt(csvLine.sourceProcess)})",
							"MATCH component -[:HAS]-(mxCredit2:MxCreditProcess) -[:HAS]-(processGroup2:ProcessGroup) -[:HAS]-(destinationProcessStep:ProcessStep{ID:toInt(csvLine.destinationProcess)})",
							"CREATE (sourceProcessStep) -[sf:SEQUENCE_FLOW{constrain:csvLine.constrain,property:csvLine.property,value:csvLine.value,operation:csvLine.operation}]-> (destinationProcessStep)",
							].join('\n');
		      
           var call = $http({
                method: 'POST',
                data: { query: query },
                url: '/api/processSteps/AddRulesToSteps',
                headers: { 'Content-Type': 'application/json' }
            });
            return call;            
       }; 
       
        this.GetAllProcessGroupWithProcessSteps = function(componentId) {
    	   console.log(componentId);
            var groupProceess = $http.get("/api/processSteps/ProcessGroupsWithChildProcess/"+componentId);                             
            return groupProceess;
       };
       
        
       this.GetAllCriticalityClass = function(componentID, itemID) {

		// console.log(componentId);
		var activeProcess = $http.get("/api/processSteps/getCriticality/" + componentID + "/" + itemID);
		//var activeProcess = $http.get("/api/processEngine/getProcessDetails/CC4/1");
		return activeProcess;
	};
       
       this.GetProcessList= function() {
       	
            var processList = $http.get("/api/processSteps/GetProcessStepsList");                             
            return processList;
       };
       
       this.GetStepAssociatedData = function(stepID,subComName) {         
		   
		    var query = [  "MATCH (n:ProcessStep) where  ID(n)= "+ stepID +" Match (n)-[r:HAS]-(sdata:SDATA{sub_com_name:'"+ subComName +"'}) return sdata"].join('\n');
		      
           var call = $http({
                method: 'POST',
                data: { query: query },
                url: '/api/processSteps/GetStepDataBySubComName',
                headers: { 'Content-Type': 'application/json' }
            });
            return call;            
       }; 
       
        this.AddFmeaStepData = function(processStepID,selectedSubComponent,itemID,data) {  
							
			var query = [  "MERGE (id:UniqueId{name:'UniqueFailureMode',str:'FM'})",
							"ON CREATE SET id.count = 1",
							"ON MATCH SET id.count = id.count + 1",
							"WITH id.str + id.count AS uid",
							"MATCH (pStep:ProcessStep)where  ID(pStep)= "+processStepID+" ",
							"CREATE (sdata:SDATA {",
							"sub_com_name :'"+ selectedSubComponent+"',",
							"failure_mode:uid,",
							"item_id:'"+itemID+"',",
							"defect_description:'"+data.defect_description+"',",
							"defect_effects:'"+ data.defect_effects +"',",
							"component_reliability:'"+ data.component_reliability +"'})",
							"CREATE(pStep)-[:HAS]-> (sdata) return sdata"
							].join('\n');
				
		      
           var call = $http({
                method: 'POST',
                data: { query: query },
                url: '/api/processSteps/AddStepData',
                headers: { 'Content-Type': 'application/json' }
            });
            return call;
       };
       
       this.AddFmecaStepData = function(processStepID,selectedSubComponent,itemID,data) {         
		
							
			var query = [  "MERGE (id:UniqueId{name:'UniqueCriticalityClass',str:'CC'})",
							"ON CREATE SET id.count = 1",
							"ON MATCH SET id.count = id.count + 1",
							"WITH id.str + id.count AS uid",
							"MATCH (pStep:ProcessStep)where  ID(pStep)= "+processStepID+" ",
							"CREATE (sdata:SDATA {",
							"sub_com_name :'"+ selectedSubComponent+"',",
							"criticality_class_id:uid,",
							"item_id:'"+itemID+"',",
							"criticality_class:'"+data.criticality_class+"',",
							"mitigation_required:'"+ data.mitigation_required +"',",
							"extra_field:'"+ data.extra_field +"'})",
							"CREATE(pStep)-[:HAS]-> (sdata) return sdata"
							].join('\n');
				
		      
           var call = $http({
                method: 'POST',
                data: { query: query },
                url: '/api/processSteps/AddStepData',
                headers: { 'Content-Type': 'application/json' }
            });
            return call;
       };
       
       this.AddRcmData = function(processStepID,selectedSubComponent,itemID,data,criticality_class_id) {         
		
							
			var query = [  	"MATCH (pStep:ProcessStep)where  ID(pStep)= "+processStepID+" ",
							"CREATE (sdata:SDATA {",
							"sub_com_name :'"+ selectedSubComponent+"',",
							"item_id:'"+itemID+"',",
							"hidden_evident_failure:'"+data.hidden_evident_failure+"',",
							"end_effects:'"+ data.end_effects +"',",
							"mitigation_method:'"+ data.mitigation_method +"',",
							"mitigation_evaluation:'"+data.mitigation_evaluation+"',",
							"fsa_msa_impact:'"+ data.fsa_msa_impact +"',",
							"mx_concept:'"+ data.mx_concept +"',",
							"criticality_class_id:'"+ criticality_class_id +"'})",
							"CREATE(pStep)-[:HAS]-> (sdata) return sdata"
							].join('\n');
				
		      
           var call = $http({
                method: 'POST',
                data: { query: query },
                url: '/api/processSteps/AddStepData',
                headers: { 'Content-Type': 'application/json' }
            });
            return call;
       };
       
       
});