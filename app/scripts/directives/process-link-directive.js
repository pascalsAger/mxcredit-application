angular.module('mctApp').directive('processLink', function() {
	return {
		restrict : 'E',
		scope:{
			componentid: '=',
			processid:'='
		},

		controller : function($scope,LinkService,$modal) {
          //  console.log("---------------ddddddddddddddddd-------------");
			
			$scope.customLink = false;
			$scope.toggleCustomLink = function() {
				console.log($scope.process);
				console.log($scope.d);
				$scope.customLink = $scope.customLink === false ? true : false;
			};
			
			
			$scope.LoadLinks = function() {
				LinkService.GetLinks($scope.componentid, $scope.processid).then(function(result) {
					$scope.links = result.data.responseData;
				});
			}; 

			
			$scope.addLink = function() {
				console.log($scope.url);
				console.log($scope.name);
				console.log($scope.description);
				
				LinkService.AddLink($scope.componentid, $scope.processid, $scope.url, $scope.name,$scope.description).then(function(result) {
					console.log(result);
					$scope.LoadLinks();					
				});
			};
			
			var ModalInstanceCtrl = function($scope, $modalInstance, linkID, LinkService) {
				$scope.type = "Data Source";
				$scope.name = "Data Source";
				$scope.ok = function() {

					LinkService.DeleteLinkById(linkID).then(function(result) {
						console.log(result);
						$modalInstance.close();
					});
				};
				$scope.cancel = function() {
					$modalInstance.dismiss('cancel');
				};
			};

			/**
			 * Open Modal
			 */
			$scope.open = function(linkID) {
				var modalInstance = $modal.open({
					templateUrl : 'views/modals/delete_modal.html',
					controller : ModalInstanceCtrl,
					resolve : {
						linkID : function() {
							return linkID;
						}
					}
				});
				modalInstance.result.then(function() {
					$scope.LoadLinks();
				}, function() {
				});
			};
			$scope.LoadLinks();
		},

		templateUrl : "views/directives/process_links.html"

	};
}); 