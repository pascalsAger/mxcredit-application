angular.module('mctApp').directive('taskSelection', function () {
    return {
        restrict: 'E',

        templateUrl: "views/directives/selectTaskOption.html"

    };
});