
angular.module('mctApp').directive('processFile', function() {
	return {
		restrict : 'E',
		scope:{
			componentid: '=',
			processid:'='
		},

		controller : function($scope,Upload,FileService,$modal) {           
            
			$scope.LoadFiles = function() {
				FileService.GetFiles($scope.componentid, $scope.processid).then(function(result) {
					$scope.files = result.data.responseData;
				});
			}; 
            
			$scope.uploadFiles = function(file, errFiles) {
				$scope.f = file;
				$scope.errFile = errFiles && errFiles[0];
				if (file) {
					 $scope.fileNameCustom = $scope.componentid + '_' + $scope.processid + '_' + file.name;
					file.upload = Upload.upload({
						url : "/api/document/"+$scope.componentid+"/"+$scope.processid,
						data : {
							file : file
						}
					});
					
					file.upload.then(function(response) {

						FileService.AddFile($scope.componentid, $scope.processid, file.name, $scope.fileNameCustom).then(function(result) {
							console.log(result);
							$scope.LoadFiles();
							//$scope.toggleCustomDocument();
						});
					}); 
				}
			};
			
			$scope.customDocument = false;
			$scope.toggleCustomDocument = function() {
				console.log($scope.processInfo);
				$scope.customDocument = $scope.customDocument === false ? true : false;
			};
			
			  
			var ModalInstanceCtrl = function($scope, $modalInstance, fileID, FileService) {
				$scope.type = "Document";
				$scope.name = "Document";
				$scope.ok = function() {

					FileService.DeleteFileById(fileID).then(function(result) {
						console.log(result);
						$modalInstance.close();
					});
				};
				$scope.cancel = function() {
					$modalInstance.dismiss('cancel');
				};
			};

			/**
			 * Open Modal
			 */
			$scope.open = function(fileID) {
				var modalInstance = $modal.open({
					templateUrl : 'views/modals/delete_modal.html',
					controller : ModalInstanceCtrl,
					resolve : {
						fileID : function() {
							return fileID;
						}
					}
				});
				modalInstance.result.then(function() {
					$scope.LoadFiles();
				}, function() {
				});
			}; 			
			
			$scope.LoadFiles();
		},

		templateUrl : "views/directives/process_files.html"

	};
}); 