
1. Intruduction
2. Fundamentals
3. Related Work
4. Solution Approach
5. Evaluation
6. Future Work
7.Conclusion                              
-----------------------------------------------------------------------------------------------------------------------------------------------
 1  Introduction
 2  Requirements Collection
     Requirements Analysis
     Integrated System Health Mgmt
     Available Maintenance Concepts
     Available Systems
3.  Concept Description
    Weak point of existing RCM Process
    Purpose of introducing new ISHM systems into A/C
    Description of Mx Credit
    Description of Mx Credit Process  
4.  Software Design and Implementation
5. Evaluation
6. Future Work
7.  Conclusion


--------------------------------------------------------------------------------------------------------------------------------------------------------------------
I. Introduction 1
1. Introduction and Motivation 3
1.1. Social Applications and Enterprise Systems . . . . . . . . . . . . . . . . . . 3
1.2. Enterprise System Data Modeling . . . . . . . . . . . . . . . . . . . . . . . 4
1.3. Motivation of this thesis . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 5
II. Foundations 7
2. Data Modeling and User Interface Complexity 11
2.1. Data Modeling . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 11
2.1.1. Data modeling process . . . . . . . . . . . . . . . . . . . . . . . . . . 11
2.1.2. Meta-Model, Model and Data . . . . . . . . . . . . . . . . . . . . . . 12
2.1.3. Model and Data Integrity . . . . . . . . . . . . . . . . . . . . . . . . . 13
2.2. User Interface Complexity . . . . . . . . . . . . . . . . . . . . . . . . . . . . 14
2.3. Integrated Modeling Environment . . . . . . . . . . . . . . . . . . . . . . . 16
2.3.1. Requirements . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 17
2.3.2. Scenarios . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 19
3. Existing Platforms 23
3.1. Drupal . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 23
3.2. Podio . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 25
3.3. Tricia . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 27
3.4. SocioCortex . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 28
4. Web Technologies 31
4.1. REST . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 31
4.2. AngularJS . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 33
4.3. Angular Material . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 34
5. Preliminary Conclusions 37
xi
Contents
III.Software Design and Implementation 39
6. Software Design 41
6.1. SocioCortex . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 41
6.1.1. Architecture . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 41
6.1.2. RESTful API . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 42
6.1.3. Types and Properties . . . . . . . . . . . . . . . . . . . . . . . . . . . 45
6.2. Integrated Modeling Environment Client . . . . . . . . . . . . . . . . . . . 47
6.2.1. Architecture . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 47
7. IME User Interface 51
7.1. Navigation . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 51
7.2. Dashboards . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 53
7.3. Modeling Environment Interface . . . . . . . . . . . . . . . . . . . . . . . . 56
IV. Results 63
8. Evaluation 65
8.1. Quantitative Evaluation . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 65
8.2. Qualitative Evaluation . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 66
9. Conclusion and Future Work 69
9.1. Conclusion . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 69
9.2. Future Work . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 70
9.2.1. Data Import . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 70
9.2.2. Live checks of inconsistencies . . . . . . . . . . . . . . . . . . . . . . 71
9.2.3. Derived Attributes . . . . . . . . . . . . . . . . . . . . . . . . . . . . . 72
Appendix 77
A. Detailed Description of Workflow Study 77
Bibliography 81